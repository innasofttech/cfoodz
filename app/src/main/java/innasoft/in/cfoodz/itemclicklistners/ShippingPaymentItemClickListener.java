package innasoft.in.cfoodz.itemclicklistners;

import android.view.View;

public interface ShippingPaymentItemClickListener
{
    void onItemClick(View v, int pos);
}
