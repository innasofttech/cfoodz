package innasoft.in.cfoodz.itemclicklistners;

import android.view.View;

public interface CityProfileItemClickListener
{
    void onItemClick(View v, int pos);
}
