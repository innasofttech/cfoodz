package innasoft.in.cfoodz.itemclicklistners;

import android.view.View;

public interface ReceipeListItemClickListener
{
    void onItemClick(View v, int pos);
}
