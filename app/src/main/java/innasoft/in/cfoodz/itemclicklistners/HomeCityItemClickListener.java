package innasoft.in.cfoodz.itemclicklistners;

import android.view.View;

public interface HomeCityItemClickListener
{
   void onItemClick(View v, int pos);
}
