package innasoft.in.cfoodz.itemclicklistners;

import android.view.View;

public interface MainCategoryItemClickListener
{
    void onItemClick(View v, int pos);
}
