package innasoft.in.cfoodz.itemclicklistners;

import android.view.View;

public interface AreaItemClickListener
{
    void onItemClick(View v, int pos);
}
