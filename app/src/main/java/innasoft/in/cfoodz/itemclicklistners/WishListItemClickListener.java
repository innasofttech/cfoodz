package innasoft.in.cfoodz.itemclicklistners;

import android.view.View;

public interface WishListItemClickListener
{
    void onItemClick(View v, int pos);
}
