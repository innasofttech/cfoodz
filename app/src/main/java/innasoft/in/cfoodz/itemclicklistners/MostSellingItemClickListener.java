package innasoft.in.cfoodz.itemclicklistners;

import android.view.View;

/**
 * Created by purushotham on 13/12/16.
 */

public interface MostSellingItemClickListener
{
    void onItemClick(View v, int pos);
}

