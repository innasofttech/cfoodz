package innasoft.in.cfoodz.itemclicklistners;

import android.view.View;

public interface CartListItemClickListener
{
    void onItemClick(View v, int pos);
}
