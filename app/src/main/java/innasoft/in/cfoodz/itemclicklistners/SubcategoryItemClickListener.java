package innasoft.in.cfoodz.itemclicklistners;

import android.view.View;

public interface SubcategoryItemClickListener
{
    void onItemClick(View v, int pos);
}
