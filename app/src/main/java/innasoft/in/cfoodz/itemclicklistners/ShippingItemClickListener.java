package innasoft.in.cfoodz.itemclicklistners;

import android.view.View;

public interface ShippingItemClickListener
{
    void onItemClick(View v, int pos);
}
