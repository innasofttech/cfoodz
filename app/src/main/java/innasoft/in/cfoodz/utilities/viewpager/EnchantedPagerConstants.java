package innasoft.in.cfoodz.utilities.viewpager;

/**
 * Created by purushotham on 20/07/2017.
 */
public class EnchantedPagerConstants {

    public final static float BIG_SCALE = 1.0f;
    public final static float SMALL_SCALE = 0.9f;
    public final static float DIFF_SCALE = BIG_SCALE - SMALL_SCALE;
}
