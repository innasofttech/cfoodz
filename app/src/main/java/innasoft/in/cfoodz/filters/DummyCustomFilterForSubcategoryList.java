package innasoft.in.cfoodz.filters;

import android.widget.Filter;

import java.util.ArrayList;

import innasoft.in.cfoodz.adapter.DummySubcategoryAdapter;
import innasoft.in.cfoodz.adapter.SubcategoryAdapter;
import innasoft.in.cfoodz.models.SubcategoryModel;

public class DummyCustomFilterForSubcategoryList extends Filter {

    DummySubcategoryAdapter adapter;
    ArrayList<SubcategoryModel> filterList;

    public DummyCustomFilterForSubcategoryList(ArrayList<SubcategoryModel> filterList, DummySubcategoryAdapter adapter) {
        this.adapter = adapter;
        this.filterList = filterList;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results=new FilterResults();
        if(constraint != null && constraint.length() > 0)
        {
            constraint=constraint.toString().toUpperCase();

            ArrayList<SubcategoryModel> filteredPlayers=new ArrayList<SubcategoryModel>();

            for (int i=0;i<filterList.size();i++)
            {
                if(filterList.get(i).getName().toUpperCase().contains(constraint) )
                {
                    filteredPlayers.add(filterList.get(i));
                }
            }
            results.count=filteredPlayers.size();
            results.values=filteredPlayers;
        }else
        {
            results.count=filterList.size();
            results.values=filterList;
        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        adapter.subcategoryModels = (ArrayList<SubcategoryModel>) results.values;
        adapter.notifyDataSetChanged();
    }
}
