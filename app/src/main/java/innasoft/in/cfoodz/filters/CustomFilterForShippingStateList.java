package innasoft.in.cfoodz.filters;

import android.widget.Filter;

import java.util.ArrayList;

import innasoft.in.cfoodz.adapter.ShippingStateAdapter;
import innasoft.in.cfoodz.models.StateModel;

public class CustomFilterForShippingStateList extends Filter {

    ShippingStateAdapter adapter;
    ArrayList<StateModel> filterList;

    public CustomFilterForShippingStateList(ArrayList<StateModel> filterList, ShippingStateAdapter adapter) {
        this.adapter = adapter;
        this.filterList = filterList;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results=new FilterResults();
        if(constraint != null && constraint.length() > 0)
        {
            constraint=constraint.toString().toUpperCase();

            ArrayList<StateModel> filteredPlayers=new ArrayList<StateModel>();

            for (int i=0;i<filterList.size();i++)
            {
                if(filterList.get(i).getName().toUpperCase().contains(constraint))
                {
                    filteredPlayers.add(filterList.get(i));
                }
            }
            results.count=filteredPlayers.size();
            results.values=filteredPlayers;
        }else
        {
            results.count=filterList.size();
            results.values=filterList;
        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        adapter.stateModels = (ArrayList<StateModel>) results.values;
        adapter.notifyDataSetChanged();
    }
}
