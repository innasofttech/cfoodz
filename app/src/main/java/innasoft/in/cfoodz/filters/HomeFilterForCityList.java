package innasoft.in.cfoodz.filters;

import android.widget.Filter;

import java.util.ArrayList;

import innasoft.in.cfoodz.adapter.HomeCityAdapter;
import innasoft.in.cfoodz.models.CityModel;


public class HomeFilterForCityList extends Filter {

    HomeCityAdapter adapter;
    ArrayList<CityModel> filterList;

    public HomeFilterForCityList(ArrayList<CityModel> filterList, HomeCityAdapter adapter) {
        this.adapter = adapter;
        this.filterList = filterList;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results=new FilterResults();
        if(constraint != null && constraint.length() > 0)
        {
            constraint=constraint.toString().toUpperCase();

            ArrayList<CityModel> filteredPlayers=new ArrayList<CityModel>();

            for (int i=0;i<filterList.size();i++)
            {
                if(filterList.get(i).getCityName().toUpperCase().contains(constraint))
                {
                    filteredPlayers.add(filterList.get(i));
                }
            }
            results.count=filteredPlayers.size();
            results.values=filteredPlayers;
        }else
        {
            results.count=filterList.size();
            results.values=filterList;
        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results)
    {
        adapter.homecityModels = (ArrayList<CityModel>) results.values;
        adapter.notifyDataSetChanged();
    }
}
