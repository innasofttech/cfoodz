package innasoft.in.cfoodz.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import java.util.ArrayList;

import innasoft.in.cfoodz.R;
import innasoft.in.cfoodz.activities.DummyFilterActivity;
import innasoft.in.cfoodz.activities.MainCategoryProductsActivity;
import innasoft.in.cfoodz.filters.CustomFilterForSubcategoryList;
import innasoft.in.cfoodz.filters.DummyCustomFilterForSubcategoryList;
import innasoft.in.cfoodz.holder.SubcategoryHolder;
import innasoft.in.cfoodz.itemclicklistners.SubcategoryItemClickListener;
import innasoft.in.cfoodz.models.SubcategoryModel;

public class DummySubcategoryAdapter extends RecyclerView.Adapter<SubcategoryHolder>implements Filterable {
    public ArrayList<SubcategoryModel> subcategoryModels,filterList;
    public DummyFilterActivity context;
    DummyCustomFilterForSubcategoryList filter;
    LayoutInflater li;
    int resource;
    Typeface typeface;

    public DummySubcategoryAdapter(ArrayList<SubcategoryModel> subcategoryModels, DummyFilterActivity context, int resource) {
        this.subcategoryModels = subcategoryModels;
        this.context = context;
        this.resource = resource;
        this.filterList = subcategoryModels;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        typeface = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.fonttype_one));

        String className = this.getClass().getCanonicalName();
        Log.d("CURRENTCLASSNAME" , className);
    }

    @Override
    public Filter getFilter() {
        if(filter==null)
        {
            filter=new DummyCustomFilterForSubcategoryList(filterList,this);
        }

        return filter;
    }

    @Override
    public SubcategoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource,parent,false);
        SubcategoryHolder slh = new SubcategoryHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(SubcategoryHolder holder, final int position) {

        holder.subcategory_name_txt.setText(subcategoryModels.get(position).getName());
        holder.subcategory_name_txt.setTypeface(typeface);

        holder.setItemClickListener(new SubcategoryItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
                context.setSubcategoryName(subcategoryModels.get(pos).getName(),subcategoryModels.get(pos).getId(),subcategoryModels.get(pos).getChild_available());

            }
        });
    }

    @Override
    public int getItemCount() {
        return this.subcategoryModels.size();
    }
}
