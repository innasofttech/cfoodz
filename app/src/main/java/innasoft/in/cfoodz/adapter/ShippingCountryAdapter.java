package innasoft.in.cfoodz.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import java.util.ArrayList;

import innasoft.in.cfoodz.R;
import innasoft.in.cfoodz.activities.ShippingProductActivity;
import innasoft.in.cfoodz.activities.UserProfileActivity;
import innasoft.in.cfoodz.filters.CustomFilterForCountryList;
import innasoft.in.cfoodz.filters.CustomFilterForShippingCountryList;
import innasoft.in.cfoodz.holder.CountryHolder;
import innasoft.in.cfoodz.itemclicklistners.CountryItemClickListener;
import innasoft.in.cfoodz.models.CountriesModel;

public class ShippingCountryAdapter extends RecyclerView.Adapter<CountryHolder>implements Filterable {
    public ArrayList<CountriesModel> countryModels,filterList;
    public ShippingProductActivity context;
    CustomFilterForShippingCountryList filter;
    LayoutInflater li;
    int resource;
    Typeface typeface;

    public ShippingCountryAdapter(ArrayList<CountriesModel> countryModels, ShippingProductActivity context, int resource) {
        this.countryModels = countryModels;
        this.context = context;
        this.resource = resource;
        this.filterList = countryModels;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        typeface = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.fonttype_one));

        String className = this.getClass().getCanonicalName();
        Log.d("CURRENTCLASSNAME" , className);
    }

    @Override
    public Filter getFilter() {
        if(filter==null)
        {
            filter=new CustomFilterForShippingCountryList(filterList,this);
        }

        return filter;
    }

    @Override
    public CountryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource,parent,false);
        CountryHolder slh = new CountryHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(CountryHolder holder, final int position) {

        holder.country_name_txt.setText(countryModels.get(position).getName());
        holder.country_name_txt.setTypeface(typeface);

        holder.setItemClickListener(new CountryItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
                //context.setCountryName(countryModels.get(pos).getName(),countryModels.get(pos).getId());

            }
        });
    }

    @Override
    public int getItemCount() {
        return this.countryModels.size();
    }
}
