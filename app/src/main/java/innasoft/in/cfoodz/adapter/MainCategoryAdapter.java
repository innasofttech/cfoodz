package innasoft.in.cfoodz.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import innasoft.in.cfoodz.R;
import innasoft.in.cfoodz.activities.MainCategoryProductsActivity;
import innasoft.in.cfoodz.fragments.HomeFragment;
import innasoft.in.cfoodz.holder.MainCategoryHolder;
import innasoft.in.cfoodz.itemclicklistners.MainCategoryItemClickListener;
import innasoft.in.cfoodz.models.MainCategoryModel;
import innasoft.in.cfoodz.utilities.UserSessionManager;

public class MainCategoryAdapter extends RecyclerView.Adapter<MainCategoryHolder> {

    private ArrayList<MainCategoryModel> mainCategListModel;
    HomeFragment context;
    LayoutInflater li;
    int resource;
    String user_id, jwt_token, page_no;
    private boolean checkInternet;
    UserSessionManager session;
    ProgressDialog progressDialog;
    Typeface typeface;


    public MainCategoryAdapter(ArrayList<MainCategoryModel> mainCategListModel, HomeFragment context, int resource) {
        this.mainCategListModel = mainCategListModel;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        typeface = Typeface.createFromAsset(context.getActivity().getAssets(), context.getResources().getString(R.string.fonttype_two));

    }

    @NonNull
    @Override
    public MainCategoryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutSeedCategory = li.inflate(resource, null);
        return new MainCategoryHolder(layoutSeedCategory);
    }

    @Override
    public void onBindViewHolder(@NonNull MainCategoryHolder holder, final int position) {


        progressDialog = new ProgressDialog(context.getActivity());
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);


        final String get_id = mainCategListModel.get(position).getCategoryId();
        String str = mainCategListModel.get(position).getCategoryName();
        String converted_string = str.substring(0, 1).toUpperCase() + str.substring(1);
        holder.category_text.setText(converted_string);
        holder.category_text.setTypeface(typeface);

        Log.d("VALUES:", str + "" + converted_string);

        if (str.equalsIgnoreCase("Fish")) {

            Picasso.with(context.getActivity())
                    .load(R.drawable.fish_image)
                    .into(holder.category_img);
        }

        if (str.equalsIgnoreCase("Prawn")) {

            Picasso.with(context.getActivity())
                    .load(R.drawable.prawn_image)
                    .into(holder.category_img);
        }

        if (str.equalsIgnoreCase("Crab")) {

            Picasso.with(context.getActivity())
                    .load(R.drawable.crab_image)
                    .into(holder.category_img);
        }


     /*   Picasso.with(context.getActivity())
                .load(R.drawable.nodata_image)
                .placeholder(R.drawable.nodata_image)
                .into(holder.category_img);*/


        holder.setItemClickListener(new MainCategoryItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
                if (get_id.equals("1")) {
                    Intent intent = new Intent(context.getActivity(), MainCategoryProductsActivity.class);
                    intent.putExtra("cat_id", "1");
                    context.startActivity(intent);
                }
                if (get_id.equals("2")) {
                    Intent intent = new Intent(context.getActivity(), MainCategoryProductsActivity.class);
                    intent.putExtra("cat_id", "2");
                    context.startActivity(intent);
                }
                if (get_id.equals("3")) {
                    Intent intent = new Intent(context.getActivity(), MainCategoryProductsActivity.class);
                    intent.putExtra("cat_id", "3");
                    context.startActivity(intent);
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return this.mainCategListModel.size();
    }


}
