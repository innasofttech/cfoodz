package innasoft.in.cfoodz.adapter;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import innasoft.in.cfoodz.R;
import innasoft.in.cfoodz.models.MainCategoryModel;


public class SlidingImage_Adapter extends PagerAdapter {


    private ArrayList<MainCategoryModel> mainCategListModel;
    private LayoutInflater inflater;
    private Context context;


    public SlidingImage_Adapter(Context context, ArrayList<MainCategoryModel> mainCategList) {
        this.context = context;
        this.mainCategListModel = mainCategList;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return mainCategListModel.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View imageLayout = inflater.inflate(R.layout.slidingimages_layout, view, false);

        assert imageLayout != null;

        TextView category_text = (TextView) imageLayout.findViewById(R.id.category_text);
        final ImageView imageView = (ImageView) imageLayout.findViewById(R.id.image);

        final String get_id = mainCategListModel.get(position).getCategoryId();
        String str = mainCategListModel.get(position).getCategoryName();
        String converted_string = str.substring(0, 1).toUpperCase() + str.substring(1);
        category_text.setText(converted_string);

        if(str.equals("Fish"))
        {

            Picasso.with(context)
                    .load(R.drawable.fish_image)
                    .into(imageView);
        }

        if(str.equals("Prawn"))
        {

            Picasso.with(context)
                    .load(R.drawable.prawn_image)
                    .into(imageView);
        }

        view.addView(imageLayout, 0);

        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }


}