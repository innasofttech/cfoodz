package innasoft.in.cfoodz.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import java.util.ArrayList;

import innasoft.in.cfoodz.R;
import innasoft.in.cfoodz.activities.MainCategoryProductsActivity;
import innasoft.in.cfoodz.filters.CustomFilterForChildcategoryList;
import innasoft.in.cfoodz.filters.CustomFilterForSubcategoryList;
import innasoft.in.cfoodz.holder.ChildcategoryHolder;
import innasoft.in.cfoodz.holder.SubcategoryHolder;
import innasoft.in.cfoodz.itemclicklistners.SubcategoryItemClickListener;
import innasoft.in.cfoodz.models.SubcategoryModel;

public class ChildcategoryAdapter extends RecyclerView.Adapter<ChildcategoryHolder>implements Filterable {
    public ArrayList<SubcategoryModel> subcategoryModels,filterList;
    public MainCategoryProductsActivity context;
    CustomFilterForChildcategoryList filter;
    LayoutInflater li;
    int resource;
    Typeface typeface;

    public ChildcategoryAdapter(ArrayList<SubcategoryModel> subcategoryModels, MainCategoryProductsActivity context, int resource) {
        this.subcategoryModels = subcategoryModels;
        this.context = context;
        this.resource = resource;
        this.filterList = subcategoryModels;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        typeface = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.fonttype_two));

        String className = this.getClass().getCanonicalName();
        Log.d("CURRENTCLASSNAME" , className);
    }

    @Override
    public Filter getFilter() {
        if(filter==null)
        {
            filter=new CustomFilterForChildcategoryList(filterList,this);
        }

        return filter;
    }

    @Override
    public ChildcategoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource,parent,false);
        ChildcategoryHolder slh = new ChildcategoryHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(ChildcategoryHolder holder, final int position) {

        holder.subcategory_name_txt.setText(subcategoryModels.get(position).getName());
        holder.subcategory_name_txt.setTypeface(typeface);

        holder.setItemClickListener(new SubcategoryItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
                context.setChildcategoryName(subcategoryModels.get(pos).getName(),subcategoryModels.get(pos).getId());

            }
        });
    }

    @Override
    public int getItemCount() {
        return this.subcategoryModels.size();
    }
}
