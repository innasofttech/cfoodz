package innasoft.in.cfoodz.adapter;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import innasoft.in.cfoodz.R;
import innasoft.in.cfoodz.fragments.HomeFragment;
import innasoft.in.cfoodz.models.GetBannerModel;

/**
 * Created by Devolper on 02-Nov-17.
 */

public class SliderPagerAdapter extends PagerAdapter
{


    private HomeFragment context;
    private ArrayList<GetBannerModel> dataObjectList;
    private LayoutInflater layoutInflater;


    public SliderPagerAdapter(HomeFragment context, ArrayList<GetBannerModel> dataObjectList) {
        this.context = context;
        this.dataObjectList = dataObjectList;
        this.layoutInflater = (LayoutInflater)this.context.getActivity().getSystemService(this.context.getActivity().LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return dataObjectList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((View)object);
    }

    @Override
    public Object instantiateItem(final ViewGroup container, final int position) {



        View view = this.layoutInflater.inflate(R.layout.row_slider_home_page, container, false);

        ImageView slider_image_iv = (ImageView) view.findViewById(R.id.slider_image_iv);

        Picasso.with(context.getActivity())
                .load(dataObjectList.get(position).getImage())
                .placeholder(R.drawable.nodata_image)
                .fit()
                .into(slider_image_iv);



        container.addView(view);


        return view;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
