package innasoft.in.cfoodz.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

import innasoft.in.cfoodz.R;
import innasoft.in.cfoodz.activities.ProductDescriptionActivity;
import innasoft.in.cfoodz.holder.MostSellingHolder;
import innasoft.in.cfoodz.holder.ReviewsHolder;
import innasoft.in.cfoodz.itemclicklistners.MostSellingItemClickListener;
import innasoft.in.cfoodz.models.MostSellingModel;
import innasoft.in.cfoodz.models.ReviewsModel;
import innasoft.in.cfoodz.utilities.CitySelectionSession;

public class ReviewsAdapter extends RecyclerView.Adapter<ReviewsHolder>{

    private ArrayList<ReviewsModel> seedCategoryList;
    ProductDescriptionActivity context;
    LayoutInflater li;
    int resource;
    Typeface typeface;
    CitySelectionSession citySelectionSession;

    Boolean isOnline = false;
    private boolean statusFlag;
    private int lastPosition = -1;
    public String disp_city_name = "", disp_city_id="";
    public ReviewsAdapter(ArrayList<ReviewsModel> seedCategoryList, ProductDescriptionActivity context, int resource) {
        this.seedCategoryList = seedCategoryList;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        typeface = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.fonttype_one));
        citySelectionSession = new CitySelectionSession(context);
        HashMap<String, String> cityDetails = citySelectionSession.getCityDetails();
        disp_city_id = cityDetails.get(CitySelectionSession.CITY_ID);
        disp_city_name = cityDetails.get(CitySelectionSession.CITY_NAME);
    }

    @Override
    public ReviewsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutSeedCategory = li.inflate(resource,null);
        ReviewsHolder sch = new ReviewsHolder(layoutSeedCategory);
        return sch;
    }

    @Override
    public void onBindViewHolder(ReviewsHolder holder, final int position) {

        // Here you apply the animation when the view is bound
       // setAnimation(holder.itemView, position);

        String str = seedCategoryList.get(position).getUser_name();
        String converted_string = str.substring(0, 1).toUpperCase() + str.substring(1);
        holder.name.setText(converted_string);
        holder.name.setTypeface(typeface);

        holder.review_text.setText(seedCategoryList.get(position).getReview());
        holder.review_text.setTypeface(typeface);

        holder.rating_bar.setRating(Float.parseFloat(seedCategoryList.get(position).getRating()));


        holder.setItemClickListener(new MostSellingItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {

            }
        });
    }


    @Override
    public int getItemCount() {
        return this.seedCategoryList.size();
    }

    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}
