package innasoft.in.cfoodz.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import innasoft.in.cfoodz.R;
import innasoft.in.cfoodz.activities.FilterActivity;
import innasoft.in.cfoodz.fragments.HomeFragment;
import innasoft.in.cfoodz.holder.FilterHolder;
import innasoft.in.cfoodz.holder.MostSellingHolder;
import innasoft.in.cfoodz.itemclicklistners.FilterClickListener;
import innasoft.in.cfoodz.itemclicklistners.MostSellingItemClickListener;
import innasoft.in.cfoodz.models.FilterModel;
import innasoft.in.cfoodz.models.MostSellingModel;

public class FilterAdapter extends RecyclerView.Adapter<FilterHolder>{

    private ArrayList<FilterModel> seedCategoryList;
    FilterActivity context;
    LayoutInflater li;
    int resource;
    Typeface typeface;
    String checkboxvalue;
    ArrayList<String> lstChk= new ArrayList<>();
    Boolean isOnline = false;
    private boolean statusFlag;
    private FilterClickListener clickListener;
    private int lastPosition = -1;
    public FilterAdapter(ArrayList<FilterModel> seedCategoryList, FilterActivity context, int resource) {
        this.seedCategoryList = seedCategoryList;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        typeface = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.fonttype_one));
    }

    @Override
    public FilterHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutSeedCategory = li.inflate(resource,null);
        FilterHolder sch = new FilterHolder(layoutSeedCategory);
        return sch;
    }

    @Override
    public void onBindViewHolder(final FilterHolder holder, final int position) {

        // Here you apply the animation when the view is bound
       // setAnimation(holder.itemView, position);
        if(lstChk.contains(seedCategoryList.get(position).getBrand_name()))
        {
            holder.check_box.setChecked(true);
        }
        holder.check_box.setText(seedCategoryList.get(position).getBrand_name());
      // holder.check_box.setText(seedCategoryList.get(position).getBrand_name());
        holder.check_box.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(compoundButton.isChecked())
                {
                    compoundButton.setChecked(true);
                    lstChk.add(seedCategoryList.get(position).getWeight_id());
                    holder.check_box.setChecked(true);
                    Intent intent=new Intent();
                    String weight_id = ""+lstChk;
                   // String ids = ((Activity) activity).getIntent().getExtras().getString("id");
                    intent.putExtra("weights",weight_id.replace("[","").replace("]",""));
                    context.setResult(2,intent);
                    context.finish();
                    Log.d("svgjsfkbvh",""+lstChk);
                    Toast.makeText(context, ""+lstChk, Toast.LENGTH_SHORT).show();

                }
                else
                {
                    compoundButton.setChecked(false);
                    lstChk.remove(seedCategoryList.get(position).getWeight_id());
                    holder.check_box.setChecked(false);
                }
            }
        });

        holder.setItemClickListener(new MostSellingItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {

            }
        });
    }


    @Override
    public int getItemCount() {
        return this.seedCategoryList.size();
    }

    public void setClickListener(FilterClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }

    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}
