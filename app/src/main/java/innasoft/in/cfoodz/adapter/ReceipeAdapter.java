package innasoft.in.cfoodz.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

import innasoft.in.cfoodz.R;
import innasoft.in.cfoodz.activities.BlogDescriptionActivity;
import innasoft.in.cfoodz.activities.BlogsListActivity;
import innasoft.in.cfoodz.activities.ProductDescriptionActivity;
import innasoft.in.cfoodz.activities.ReceipeActivity;
import innasoft.in.cfoodz.holder.BlogListHolder;
import innasoft.in.cfoodz.holder.ReceipeListHolder;
import innasoft.in.cfoodz.itemclicklistners.BlogListItemClickListener;
import innasoft.in.cfoodz.itemclicklistners.ReceipeListItemClickListener;
import innasoft.in.cfoodz.models.BlogListModel;
import innasoft.in.cfoodz.models.RecepieModel;
import innasoft.in.cfoodz.utilities.CitySelectionSession;

public class ReceipeAdapter extends RecyclerView.Adapter<ReceipeListHolder>{

    private ArrayList<RecepieModel> receipeList;
    ProductDescriptionActivity context;
    LayoutInflater li;
    int resource;
    String disp_city_id,recep_id,product_id;
    Typeface typeface;
    CitySelectionSession citySelectionSession;

    public ReceipeAdapter(ArrayList<RecepieModel> receipeList, ProductDescriptionActivity context, int resource) {
        this.receipeList = receipeList;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        typeface = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.fonttype_one));
        citySelectionSession = new CitySelectionSession(context);
        HashMap<String, String> cityDetails = citySelectionSession.getCityDetails();
        disp_city_id = cityDetails.get(CitySelectionSession.CITY_ID);
    }

    @Override
    public ReceipeListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutSeedCategory = li.inflate(resource,null);
        ReceipeListHolder sch = new ReceipeListHolder(layoutSeedCategory);
        return sch;
    }

    @Override
    public void onBindViewHolder(ReceipeListHolder holder, final int position) {



      //  recep_id=receipeList.get(position).getId();;
        //product_id=receipeList.get(position).getProduct_id();;
        String str = receipeList.get(position).getLanguage_name();
        String converted_string = str.substring(0, 1).toUpperCase() + str.substring(1);

        holder.receipe_lang_text.setText(Html.fromHtml(converted_string));
        holder.receipe_lang_text.setTypeface(typeface);

        holder.receipe_content.setText(Html.fromHtml(receipeList.get(position).getContent()));
        holder.receipe_content.setTypeface(typeface);


        holder.receipe_lang_button_more.setTypeface(typeface);


         holder.receipe_lang_button_more.setOnClickListener(new View.OnClickListener()
         {
             @Override
             public void onClick(View view)
             {
                Intent irecep=new Intent(context, ReceipeActivity.class);
                 irecep.putExtra("recipe_id",receipeList.get(position).getId());
                 irecep.putExtra("location_id",disp_city_id);
                 irecep.putExtra("product_id",receipeList.get(position).getProduct_id());
                  context.startActivity(irecep);
             }
         });


        holder.setItemClickListener(new ReceipeListItemClickListener()
        {
            @Override
            public void onItemClick(View v, int pos)
            {




            }
        });
    }


    @Override
    public int getItemCount() {
        return this.receipeList.size();
    }


}
