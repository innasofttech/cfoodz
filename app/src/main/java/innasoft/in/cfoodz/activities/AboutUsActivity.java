package innasoft.in.cfoodz.activities;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import innasoft.in.cfoodz.R;

public class AboutUsActivity extends AppCompatActivity {

    TextView about_text_title,about_text;
    Typeface typeface,typeface2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);

        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.fonttype_one));
        typeface2 = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.fonttype_two));

              about_text_title=(TextView)findViewById(R.id.about_text_title) ;
              about_text_title.setTypeface(typeface2);

              about_text=(TextView)findViewById(R.id.about_text) ;
             about_text.setTypeface(typeface);



        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("About Us");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
