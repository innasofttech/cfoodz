package innasoft.in.cfoodz.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.yahoo.mobile.client.android.util.rangeseekbar.RangeSeekBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import innasoft.in.cfoodz.R;
import innasoft.in.cfoodz.adapter.ChildcategoryAdapter;
import innasoft.in.cfoodz.adapter.DummyChildcategoryAdapter;
import innasoft.in.cfoodz.adapter.DummySubcategoryAdapter;
import innasoft.in.cfoodz.adapter.SubcategoryAdapter;
import innasoft.in.cfoodz.holder.FilterHolder;
import innasoft.in.cfoodz.itemclicklistners.FilterClickListener;
import innasoft.in.cfoodz.itemclicklistners.MostSellingItemClickListener;
import innasoft.in.cfoodz.models.FilterModel;
import innasoft.in.cfoodz.models.SubcategoryModel;
import innasoft.in.cfoodz.utilities.AppUrls;
import innasoft.in.cfoodz.utilities.CitySelectionSession;
import innasoft.in.cfoodz.utilities.NetworkChecking;

public class DummyFilterActivity extends AppCompatActivity implements  View.OnClickListener{
   RangeSeekBar seekBar;
    TextView min_price,max_price,subcategory_txt,child_category_txt;
    private boolean checkInternet;
    public String banner_mage;
    RecyclerView most_selling_recyclerview;
    ArrayList<FilterModel> mostsellinglist;
    FilterAdapter mostsellingadapter;
    LinearLayoutManager  layoutManager;
    CitySelectionSession citySelectionSession;
    public String disp_city_name = "", disp_city_id="";
    String min_val,max_val,weight_id;
    Button apply;
    Intent intent=new Intent();
    Typeface typeface;

    ////////////////////////////////////
    ProgressDialog progressDialog;
    AlertDialog subcategorydialog;
    AlertDialog Childcategorydialog;
    ImageView image_grid;
    String sub_category_id = "0";
    /*Countrys List*/
    DummySubcategoryAdapter subcategoryAdapter;
    DummyChildcategoryAdapter childcategoryAdapter;
    ArrayList<SubcategoryModel> subcategoryModels = new ArrayList<SubcategoryModel>();
    ArrayList<SubcategoryModel> child_cat_models = new ArrayList<SubcategoryModel>();
    ArrayList<String> subcategoryList = new ArrayList<String>();
    ArrayList<String> childcategoryList = new ArrayList<String>();
    String send_subcategory_id = "";
    String child_avil_id = "0",child_category_id = "0";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        citySelectionSession = new CitySelectionSession(DummyFilterActivity.this);

        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.fonttype_one));

        HashMap<String, String> cityDetails = citySelectionSession.getCityDetails();
        disp_city_id = cityDetails.get(CitySelectionSession.CITY_ID);
        disp_city_name = cityDetails.get(CitySelectionSession.CITY_NAME);

        //   disp_lng = cityDetails.get(CitySelectionSession.CITY_LAT);
        //   disp_lng = cityDetails.get(CitySelectionSession.CITY_LNG);
        try{


        }catch (NullPointerException e){
            String msg = (e.getMessage()==null)?"Login failed!":e.getMessage();
            Log.i("Login Error1",msg);
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Filter");
        most_selling_recyclerview = (RecyclerView)findViewById(R.id.weight_recyclerview);
        mostsellinglist = new ArrayList<FilterModel>();
        mostsellingadapter = new FilterAdapter(mostsellinglist, DummyFilterActivity.this, R.layout.filter_row);
        layoutManager = new LinearLayoutManager(this);
        most_selling_recyclerview.setNestedScrollingEnabled(false);
        most_selling_recyclerview.setLayoutManager(layoutManager);
        min_price = (TextView)findViewById(R.id.min_price);
        max_price = (TextView)findViewById(R.id.max_price);
        apply = (Button) findViewById(R.id.apply);
        seekBar = (RangeSeekBar)findViewById(R.id.rangeSeekbar);
        seekBar.setRangeValues(0, 5000);
        min_price.setText("\u20B9 "+"0");
        min_price.setTypeface(typeface);
        min_val = "empty";
        max_price.setText("\u20B9 "+"5000");
        max_price.setTypeface(typeface);
        max_val = "empty";
        weight_id = "empty";
        seekBar.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener<Integer>() {
            @Override
            public void onRangeSeekBarValuesChanged(RangeSeekBar<?> bar, Integer minValue, Integer maxValue) {
                //Now you have the minValue and maxValue of your RangeSeekbar
            //    Toast.makeText(getApplicationContext(), minValue + "-" + maxValue, Toast.LENGTH_LONG).show();
                min_price.setText("\u20A8 "+minValue);
                max_price.setText("\u20A8 "+maxValue);
                min_val = String.valueOf(minValue);
                max_val = String.valueOf(maxValue);
            }
        });

// Get noticed while dragging
        seekBar.setNotifyWhileDragging(true);
        apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                intent.putExtra("min_price",min_val);
                intent.putExtra("max_price",max_val);
                intent.putExtra("weights",weight_id);
                intent.putExtra("sub_cat",sub_category_id);
                intent.putExtra("child_cat",child_category_id);

                Log.d("dfvdzhg",weight_id);
                setResult(2,intent);

                finish();
            }
        });

        ///////////////////////////////////////////////////
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);
        subcategory_txt = (TextView) findViewById(R.id.subcategory_txt);
        child_category_txt = (TextView) findViewById(R.id.child_category_txt);
        subcategory_txt.setOnClickListener(this);
        child_category_txt.setOnClickListener(this);
        ////////////////////////////////////////

        getWeights();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                sub_category_id = "0";
                child_category_id = "0";
                weight_id = "empty";
                min_val = "empty";
                max_val = "empty";
                intent.putExtra("min_price",min_val);
                intent.putExtra("max_price",max_val);
                intent.putExtra("weights",weight_id);
                intent.putExtra("sub_cat",sub_category_id);
                intent.putExtra("child_cat",child_category_id);
                Log.d("dfvdzhg",weight_id);
                setResult(2,intent);
                finish();
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private void getWeights() {

        checkInternet = NetworkChecking.isConnected(DummyFilterActivity.this);
        if (checkInternet) {

            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.GET_RELATED_WEIGHTS,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("ressdjfasdh",response);
                                String responceCode = jsonObject.getString("status");
                                if(responceCode.equals("10100")) {


                                    JSONObject json = jsonObject.getJSONObject("data");
                                    JSONArray jsonArray = json.getJSONArray("recordData");




                                    for (int i = 0; i < jsonArray.length(); i++) {

                                        FilterModel gbm = new FilterModel();

                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        gbm.setId(jsonObject1.getString("id"));
                                        gbm.setWeight_id(jsonObject1.getString("weight_id"));
                                        gbm.setBrand_name(jsonObject1.getString("brand_name"));
                                        gbm.setCount(jsonObject1.getString("count"));
                                        gbm.setStatus(jsonObject1.getString("status"));


                                        mostsellinglist.add(gbm);

                                    }

                                    most_selling_recyclerview.setAdapter(mostsellingadapter);

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {


                    if (error instanceof TimeoutError || error instanceof NoConnectionError)
                    {
                    } else if (error instanceof AuthFailureError)
                    {
                    } else if (error instanceof ServerError)
                    {
                    } else if (error instanceof NetworkError)
                    {
                    } else if (error instanceof ParseError)
                    {
                    }
                }
            }){


                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("location", disp_city_id);
                    params.put("cat_id", getIntent().getExtras().getString("cat_id"));
                    params.put("subcat_id", getIntent().getExtras().getString("sub_cat_id"));
                    params.put("child_id", getIntent().getExtras().getString("child_cat_id"));
                    Log.d("LoginREQUESTDATA:",params.toString());
                    return params;
                }
                       /* @Override
                        public byte[] getBody() throws AuthFailureError
                        {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("email", email);
                            params.put("password", psw);
                            Log.d("LoginREQUESTDATA ", "PARAMS " + params.toString());
                            return new JSONObject(params).toString().getBytes();
                        }*/
            };

            RequestQueue requestQueue = Volley.newRequestQueue(DummyFilterActivity.this);
            requestQueue.add(stringRequest);

        }else {
            Toast.makeText(this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onClick(View v) {
        if (v == subcategory_txt){
            subcategoryList();
        }if (v == child_category_txt){
            childCategoryList();
        }
    }

    public class FilterAdapter extends RecyclerView.Adapter<FilterHolder>{

        private ArrayList<FilterModel> seedCategoryList;
        DummyFilterActivity context;
        LayoutInflater li;
        int resource;
        String checkboxvalue;
        ArrayList<String> lstChk= new ArrayList<>();
        Boolean isOnline = false;
        private boolean statusFlag;
        private SparseBooleanArray mCheckedItems = new SparseBooleanArray();
        private FilterClickListener clickListener;
        private int lastPosition = -1;
        public FilterAdapter(ArrayList<FilterModel> seedCategoryList, DummyFilterActivity context, int resource) {
            this.seedCategoryList = seedCategoryList;
            this.context = context;
            this.resource = resource;
            li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public FilterHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View layoutSeedCategory = li.inflate(resource,null);
            FilterHolder sch = new FilterHolder(layoutSeedCategory);
            return sch;
        }

        @Override
        public void onBindViewHolder(final FilterHolder holder, final int position) {

            // Here you apply the animation when the view is bound
            // setAnimation(holder.itemView, position);
            if(lstChk.contains(seedCategoryList.get(position).getBrand_name()))
            {
                holder.check_box.setChecked(true);
            }
            holder.check_box.setText(seedCategoryList.get(position).getBrand_name());
            holder.check_box.setTypeface(typeface);
            // holder.check_box.setText(seedCategoryList.get(position).getBrand_name());
            holder.check_box.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if(compoundButton.isChecked())
                    {
                        compoundButton.setChecked(true);
                        lstChk.add(seedCategoryList.get(position).getWeight_id());
                        holder.check_box.setChecked(true);

                        String weigh_id = ""+lstChk;
                        if (weigh_id.equals(",")){
                            weight_id = "empty";
                        }
                        else {
                            weight_id = weigh_id.replace("[","").replace("]","");
                        }

                        // String ids = ((Activity) activity).getIntent().getExtras().getString("id");
                       // intent.putExtra("weights",weight_id.replace("[","").replace("]",""));

                        Log.d("svgjsfkbvh",""+lstChk);
                       // Toast.makeText(context, ""+lstChk, Toast.LENGTH_SHORT).show();

                    }
                    else
                    {
                        compoundButton.setChecked(false);
                        lstChk.remove(seedCategoryList.get(position).getWeight_id());
                        holder.check_box.setChecked(false);
                        String weigh_id = ""+lstChk;

                        if (weigh_id.equals(",")){
                            weight_id = "empty";
                        }
                        else {
                            weight_id = weigh_id.replace("[","").replace("]","");
                        }
                    }
                }
            });
            holder.check_box.setChecked(mCheckedItems.get(position));

            holder.check_box.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = holder.getAdapterPosition();
                    final boolean newValue = !holder.check_box.isChecked();

                    mCheckedItems.put(position, newValue);
                   // checkedTextView.setChecked(newValue);

                    //display the text accordingly with the newValue value
                   // Snackbar snackbar = Snackbar.make(v, "Item Favorited", Snackbar.LENGTH_SHORT);
                   // snackbar.show();
                    getSelectedItemPositions();

                }
            });

            holder.setItemClickListener(new MostSellingItemClickListener() {
                @Override
                public void onItemClick(View v, int pos) {

                }
            });


        }
        public void restoreSelectedItems(List<Integer> positions){
            for (Integer position : positions) {
                mCheckedItems.put(position, true);
            }
        }

    public List<Integer> getSelectedItemPositions() {
        List<Integer> selected = new ArrayList<>();
        restoreSelectedItems(selected);
        for (int i = 0; i < mCheckedItems.size(); i++) {
            final boolean checked = mCheckedItems.valueAt(i);
            if (checked) {
                selected.add(mCheckedItems.keyAt(i));

            }
        }
        return selected;
    }

        @Override
        public int getItemCount() {
            return this.seedCategoryList.size();
        }

        public void setClickListener(FilterClickListener itemClickListener) {
            this.clickListener = itemClickListener;
        }

        private void setAnimation(View viewToAnimate, int position)
        {
            // If the bound view wasn't previously displayed on screen, it's animated
            if (position > lastPosition)
            {
                Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
                viewToAnimate.startAnimation(animation);
                lastPosition = position;
            }
        }
    }
    private void subcategoryList() {
        subcategoryModels.clear();
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            progressDialog.show();
            Log.d("SubCategory",AppUrls.BASE_URL + AppUrls.GET_SUBCATEGORY);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.GET_SUBCATEGORY,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("SubCategoryResponce",response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")){

                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
//                                    String recordTotalCnt = jsonObject1.getString("recordTotalCnt");

                                    JSONArray jsonArray = jsonObject1.getJSONArray("recordData");
                                    progressDialog.cancel();

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        SubcategoryModel cm = new SubcategoryModel();
                                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                        cm.setId(jsonObject2.getString("id"));
                                        String subcategory_name = jsonObject2.getString("name");
                                        String child_available = jsonObject2.getString("child_available");
                                        cm.setName(subcategory_name);
                                        cm.setChild_available(child_available);

                                        subcategoryList.add(subcategory_name);
                                        subcategoryModels.add(cm);
                                    }
                                    subcategoryDialog();
                                }

                                if (responceCode.equals("12786")){
                                    Toast.makeText(DummyFilterActivity.this, "No Subcategories Found..!", Toast.LENGTH_SHORT).show();
                                }

                                if (responceCode.equals("10786")){
                                    Toast.makeText(DummyFilterActivity.this, "Invalid Token..!", Toast.LENGTH_SHORT).show();
                                }
                                if(responceCode.equals("11786"))
                                {
                                    Toast.makeText(DummyFilterActivity.this, "All Fields Required..!", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                progressDialog.cancel();
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();

                    if (error instanceof TimeoutError || error instanceof NoConnectionError)
                    {
                    } else if (error instanceof AuthFailureError)
                    {
                    } else if (error instanceof ServerError)
                    {
                    } else if (error instanceof NetworkError)
                    {
                    } else if (error instanceof ParseError)
                    {
                    }
                }
            })
            {

                @Override
                protected Map<String, String> getParams() throws AuthFailureError
                {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("main_category_id", getIntent().getExtras().getString("cat_id"));
                    params.put("page_no", "1");
                    Log.d("SubcategoryParams:",params.toString());
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);

        }else {
            Snackbar snackbar = Snackbar.make(getWindow().getDecorView().getRootView(), "No Internet Connection...!", Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }
    private void subcategoryDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();

        View dialog_layout = inflater.inflate(R.layout.subcategory_list,null);

        TextView subcategory_txt = (TextView) dialog_layout.findViewById(R.id.subcategory_txt);

        final SearchView subcategory_search = (SearchView) dialog_layout.findViewById(R.id.subcategory_search);
        EditText searchEditText = (EditText) subcategory_search.findViewById(android.support.v7.appcompat.R.id.search_src_text);

        RecyclerView subcategory_recyclerview = (RecyclerView) dialog_layout.findViewById(R.id.subcategory_recyclerview);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        subcategory_recyclerview.setLayoutManager(layoutManager);

        subcategoryAdapter = new DummySubcategoryAdapter(subcategoryModels, DummyFilterActivity.this,R.layout.row_subcategory);

        subcategory_recyclerview.setAdapter(subcategoryAdapter);
        subcategory_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                subcategory_search.setIconified(false);
            }
        });

        builder.setView(dialog_layout).setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                progressDialog.dismiss();
                dialogInterface.dismiss();
            }
        });

        subcategorydialog = builder.create();

        subcategory_search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                subcategoryAdapter.getFilter().filter(query);
                return false;
            }
        });
        subcategorydialog.show();
    }
    public void setSubcategoryName(String subcategoryName, String subcategoryId, String child_available) {

        subcategorydialog.dismiss();
        subcategoryAdapter.notifyDataSetChanged();

        String sendSubcategoryName = subcategoryName;
        subcategory_txt.setText(sendSubcategoryName);
        send_subcategory_id = subcategoryId;
        sub_category_id = send_subcategory_id;
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

        child_avil_id = child_available;
        if (!child_avil_id.equals("0")){
            child_category_txt.setVisibility(View.VISIBLE);
            // childCategoryList();
        }
        else {
            child_category_txt.setVisibility(View.GONE);
        }

    }
    private void childCategoryList() {
        child_cat_models.clear();
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            progressDialog.show();
            Log.d("SubCategory",AppUrls.BASE_URL + AppUrls.GET_SUBCATEGORY);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.GET_CHILD_CATEGORY,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("SubCategoryResponce",response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")){

                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
//                                    String recordTotalCnt = jsonObject1.getString("recordTotalCnt");

                                    JSONArray jsonArray = jsonObject1.getJSONArray("recordData");
                                    progressDialog.cancel();

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        SubcategoryModel cm = new SubcategoryModel();
                                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                        cm.setId(jsonObject2.getString("id"));
                                        String subcategory_name = jsonObject2.getString("name");

                                        cm.setName(subcategory_name);


                                        childcategoryList.add(subcategory_name);
                                        child_cat_models.add(cm);
                                    }
                                    childCategoryDialog();
                                }

                                if (responceCode.equals("12786")){
                                    Toast.makeText(DummyFilterActivity.this, "No Subcategories Found..!", Toast.LENGTH_SHORT).show();
                                }

                                if (responceCode.equals("10786")){
                                    Toast.makeText(DummyFilterActivity.this, "Invalid Token..!", Toast.LENGTH_SHORT).show();
                                }
                                if(responceCode.equals("11786"))
                                {
                                    Toast.makeText(DummyFilterActivity.this, "All Fields Required..!", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                progressDialog.cancel();
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();

                    if (error instanceof TimeoutError || error instanceof NoConnectionError)
                    {
                    } else if (error instanceof AuthFailureError)
                    {
                    } else if (error instanceof ServerError)
                    {
                    } else if (error instanceof NetworkError)
                    {
                    } else if (error instanceof ParseError)
                    {
                    }
                }
            })
            {

                @Override
                protected Map<String, String> getParams() throws AuthFailureError
                {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sub_category_id", sub_category_id);
                    // params.put("page_no", "1");
                    Log.d("SubcategoryParams:",params.toString());
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);

        }else {
            Snackbar snackbar = Snackbar.make(getWindow().getDecorView().getRootView(), "No Internet Connection...!", Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }
    private void childCategoryDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();

        View dialog_layout = inflater.inflate(R.layout.subcategory_list,null);

        TextView subcategory_txt = (TextView) dialog_layout.findViewById(R.id.subcategory_txt);

        final SearchView subcategory_search = (SearchView) dialog_layout.findViewById(R.id.subcategory_search);
        EditText searchEditText = (EditText) subcategory_search.findViewById(android.support.v7.appcompat.R.id.search_src_text);

        RecyclerView subcategory_recyclerview = (RecyclerView) dialog_layout.findViewById(R.id.subcategory_recyclerview);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        subcategory_recyclerview.setLayoutManager(layoutManager);

        childcategoryAdapter = new DummyChildcategoryAdapter(child_cat_models, DummyFilterActivity.this,R.layout.row_subcategory);

        subcategory_recyclerview.setAdapter(childcategoryAdapter);
        subcategory_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                subcategory_search.setIconified(false);
            }
        });

        builder.setView(dialog_layout).setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                progressDialog.dismiss();
                dialogInterface.dismiss();
            }
        });

        Childcategorydialog = builder.create();

        subcategory_search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                subcategoryAdapter.getFilter().filter(query);
                return false;
            }
        });
        Childcategorydialog.show();
    }
    public void setChildcategoryName(String subcategoryName, String subcategoryId) {

        Childcategorydialog.dismiss();
        subcategoryAdapter.notifyDataSetChanged();

        String sendSubcategoryName = subcategoryName;
        child_category_txt.setText(sendSubcategoryName);
        child_category_id = subcategoryId;

        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);


    }
    @Override
    public void onBackPressed() {

        sub_category_id = "0";
        child_category_id = "0";
        weight_id = "empty";
        min_val = "empty";
        max_val = "empty";
        intent.putExtra("min_price",min_val);
        intent.putExtra("max_price",max_val);
        intent.putExtra("weights",weight_id);
        intent.putExtra("sub_cat",sub_category_id);
        intent.putExtra("child_cat",child_category_id);
        Log.d("dfvdzhg",weight_id);
        setResult(2,intent);
        finish();
    }
}
