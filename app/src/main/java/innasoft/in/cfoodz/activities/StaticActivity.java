package innasoft.in.cfoodz.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;

import innasoft.in.cfoodz.MainActivity;
import innasoft.in.cfoodz.R;
import innasoft.in.cfoodz.utilities.CitySelectionSession;
import innasoft.in.cfoodz.utilities.CustomTypefaceSpan;
import innasoft.in.cfoodz.utilities.UserSessionManager;

public class StaticActivity extends AppCompatActivity
{

    Toolbar toolbar;
    ImageView cart_iv;
    private DrawerLayout drawer;
    NavigationView navigationView;
    Typeface typeface;
    TextView toolbar_title,tvSnackBar,count_tv,location;
    private Boolean exit = false;
    UserSessionManager session;
    private boolean checkInternet;
    ProgressDialog progressDialog;
    public String user_id,user_name,user_email,user_phone;
    CitySelectionSession citySelectionSession;
    public String disp_city_name = "", disp_city_id="";
  //  CartBucketDBHelper cartBucketDBHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_static);

        typeface = Typeface.createFromAsset(getAssets(), "museosanscyrl.ttf");

       // cartBucketDBHelper = new CartBucketDBHelper(StaticActivity.this);

        citySelectionSession = new CitySelectionSession(getApplicationContext());
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        session = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_name = userDetails.get(UserSessionManager.USER_NAME);
        user_email = userDetails.get(UserSessionManager.USER_EMAIL);
        user_phone = userDetails.get(UserSessionManager.USER_MOBILE);

        toolbar = (Toolbar) findViewById(R.id.toolbar);

        count_tv = (TextView) findViewById(R.id.count_tv);

        count_tv.setTypeface(typeface);

        location = (TextView) findViewById(R.id.location);

    //    String countValues = String.valueOf(cartBucketDBHelper.findNumberOfProducts());

       /* if (countValues.equals("null")|| countValues.equals("") || countValues.equals("0")){
            count_tv.setVisibility(View.GONE);
        }else {
            count_tv.setVisibility(View.VISIBLE);
            count_tv.setText(countValues);
        }*/

        location=(TextView) findViewById(R.id.location);
        location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {

                Intent intent = new Intent(StaticActivity.this,LocationDialogActivity.class);
                startActivity(intent);
            }
        });

        cart_iv = (ImageView) findViewById(R.id.cart_iv);
        cart_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(StaticActivity.this, "Check....", Toast.LENGTH_SHORT).show();
                
               /* if (session.checkLogin() != false) {

                    Intent i = new Intent(StaticActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                } else {*/
             //   String countValues = String.valueOf(cartBucketDBHelper.findNumberOfProducts());
             //   count_tv.setText(countValues);
                /*Intent intent = new Intent(StaticActivity.this, ListOfSelectedProductsActivity.class);
                intent.putExtra("ActivityType", "StaticActivity");
                startActivity(intent);*/
                //}
            }
        });

        navigationView = (NavigationView) findViewById(R.id.nav_view);
//        setSupportActionBar(toolbar);

        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        toolbar_title.setTypeface(typeface);

        tvSnackBar = (TextView) findViewById(R.id.tvSnackBar);
        tvSnackBar.setTypeface(typeface);

        drawer = (DrawerLayout) findViewById(R.id.drawerLayout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        Menu m = navigationView.getMenu();
        for (int i=0;i<m.size();i++)
        {
            MenuItem mi = m.getItem(i);

            SubMenu subMenu = mi.getSubMenu();
            if (subMenu!=null && subMenu.size() >0 ) {
                for (int j=0; j <subMenu.size();j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem);
                }
            }
            applyFontToMenuItem(mi);
        }

        if(citySelectionSession.checkCityCreated() == false)
        {
            HashMap<String, String> cityDetails = citySelectionSession.getCityDetails();
            disp_city_id = cityDetails.get(CitySelectionSession.CITY_ID);
            disp_city_name = cityDetails.get(CitySelectionSession.CITY_NAME);
            //   disp_lng = cityDetails.get(CitySelectionSession.CITY_LAT);
            //   disp_lng = cityDetails.get(CitySelectionSession.CITY_LNG);
            try{
                Log.d("FIIINDCITYLENGTH", disp_city_name);
                location.setText(disp_city_name);
            }catch (NullPointerException e){
                String msg = (e.getMessage()==null)?"Login failed!":e.getMessage();
                Log.i("Login Error1",msg);
            }
        }else {
            Intent intent = new Intent(StaticActivity.this, LocationDialogActivity.class);
            startActivity(intent);
        }

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                drawer.closeDrawers();
                if (menuItem.getItemId() == R.id.home) {

                    Intent intent = new Intent(getApplicationContext(), StaticActivity.class);
                    startActivity(intent);
                }

                if (menuItem.getItemId() == R.id.login) {

                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(intent);
                }

                if (menuItem.getItemId() == R.id.wish_list) {

                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(intent);
                }

              /*  if (menuItem.getItemId() == R.id.categories_subcategories_list) {

                    Intent intent = new Intent(getApplicationContext(), CategorySubCategoryActivity.class);
                    startActivity(intent);
                }*/

                if (menuItem.getItemId() == R.id.about_us) {

                    Intent intent = new Intent(getApplicationContext(), AboutUsActivity.class);
                    startActivity(intent);
                }

                if (menuItem.getItemId() == R.id.share)
                {

                    Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                    sharingIntent.setType("text/plain");
                    sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "cfoodz");
                    sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=innasoft.in.cfoodz&hl=en");
                    startActivity(Intent.createChooser(sharingIntent, "Share via"));
                }
                return false;
            }
        });

        View navigationheadderView = navigationView.getHeaderView(0);

    }

    private void applyFontToMenuItem(MenuItem mi) {

        Typeface font = Typeface.createFromAsset(getAssets(), "museosanscyrl.ttf");

        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("" , font), 0 , mNewTitle.length(),  Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }

    @Override
    public void onBackPressed() {

        if (drawer.isDrawerOpen(GravityCompat.START)){
            drawer.closeDrawer(GravityCompat.START);
        }else {
            if (exit) {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                moveTaskToBack(true);
            } else {
                Toast.makeText(getApplicationContext(), "Press Back again to Exit.", Toast.LENGTH_SHORT).show();
                exit = true;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        exit = false;
                    }
                }, 2 * 1000);
            }
        }
    }
}
