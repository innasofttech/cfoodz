package innasoft.in.cfoodz.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import innasoft.in.cfoodz.MainActivity;
import innasoft.in.cfoodz.R;
import innasoft.in.cfoodz.utilities.AppUrls;

public class FailureActivity extends AppCompatActivity implements View.OnClickListener{

    ImageView close;
    TextView toolbar_tittle, failure_txt, failure_txt_title;
    Typeface typeface;
    String order_pid;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_failure);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Failure");

        typeface = Typeface.createFromAsset(this.getAssets(), "museosanscyrl.ttf");
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);
        //progressDialog.setCancelable(false);

        order_pid = getIntent().getExtras().getString("order_pid");

        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);
        toolbar_tittle = (TextView) findViewById(R.id.toolbar_tittle);
        toolbar_tittle.setTypeface(typeface);
        failure_txt = (TextView) findViewById(R.id.failure_txt);
        failure_txt_title = (TextView) findViewById(R.id.failure_txt_title);
        failure_txt.setTypeface(typeface);
        failure_txt_title.setTypeface(typeface);

        paymentFailure();
    }

    private void paymentFailure() {
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.RAZORPAYFAILURE+"order_id="+order_pid,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("RESENDOTPRESP:", response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String successResponceCode = jsonObject.getString("status");
                            if (successResponceCode.equals("10100")) {

                                final String message = jsonObject.getString("message");
                                progressDialog.dismiss();
                                failure_txt.setText(message);

                                runOnUiThread(new Runnable() {
                                    public void run() {
                                        failure_txt.setText(message);
                                    }
                                });

                            }
                            if (successResponceCode.equals("10200")) {

                            }

                            if (successResponceCode.equals("10300")) {

                            }
                            if (successResponceCode.equals("11786")) {
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(FailureActivity.this);
        requestQueue.add(stringRequest);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {
        if (v == close) {
            Intent i = new Intent(FailureActivity.this, MainActivity.class);
            startActivity(i);
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(FailureActivity.this, MainActivity.class);
        startActivity(i);
        finish();
    }
}
