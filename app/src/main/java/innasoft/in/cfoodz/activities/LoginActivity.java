package innasoft.in.cfoodz.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.provider.Settings;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.florent37.materialtextfield.MaterialTextField;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import innasoft.in.cfoodz.MainActivity;
import innasoft.in.cfoodz.R;
import innasoft.in.cfoodz.utilities.AppUrls;
import innasoft.in.cfoodz.utilities.NetworkChecking;
import innasoft.in.cfoodz.utilities.UserSessionManager;


public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    TextView forgot_txt, register_here_txt, textSignIn;
    EditText username_edt, password_edt;
    TextInputLayout userNameTil, passwordTil;
    Button login_btn;
    boolean checkInternet;
    ProgressDialog progressDialog;
    UserSessionManager session;
    AppBarLayout app_bar_layout;
    Typeface typeface, typefacePoppins, typefaceSegoeUi;
    String device_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        device_id = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        Log.d("DEVICEID:", device_id);
        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.fonttype_one));
        typefacePoppins = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.poppins));
        typefaceSegoeUi = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.segoeui_bold));

        app_bar_layout = findViewById(R.id.app_bar_layout);

        session = new UserSessionManager(getApplicationContext());

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        forgot_txt = findViewById(R.id.forgot_txt);
        forgot_txt.setTypeface(typefacePoppins);
        forgot_txt.setOnClickListener(this);

        register_here_txt = findViewById(R.id.register_here_txt);
        register_here_txt.setTypeface(typeface);
        register_here_txt.setOnClickListener(this);

        String styledText = "New user? <font color='#00AFD8'><b>Sign Up</b></font>.";
        register_here_txt.setText(Html.fromHtml(styledText), TextView.BufferType.SPANNABLE);

        userNameTil = findViewById(R.id.userNameTil);
        userNameTil.setTypeface(typefacePoppins);
        textSignIn = findViewById(R.id.textSignIn);
        textSignIn.setTypeface(typefacePoppins);

        String text = "<font color='#ffffff'><b>Sign In |</b></font>  <font color='#b5b5b5'>Sign Up</font>";
        textSignIn.setText(Html.fromHtml(text), TextView.BufferType.SPANNABLE);
        textSignIn.setOnClickListener(this);

        passwordTil = findViewById(R.id.passwordTil);
        passwordTil.setTypeface(typefacePoppins);

        username_edt = findViewById(R.id.username_edt);
        username_edt.setTypeface(typefacePoppins);

        password_edt = findViewById(R.id.password_edt);
        password_edt.setTypeface(typefacePoppins);

        login_btn = findViewById(R.id.login_btn);
        login_btn.setTypeface(typefacePoppins);
        login_btn.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        if (view == login_btn) {
            if (validate()) {
                checkInternet = NetworkChecking.isConnected(this);
                if (checkInternet) {

                    if (username_edt.getText().length() > 0) {
                        if (password_edt.getText().length() > 0) {

                        }
                    }

                    final String email = username_edt.getText().toString().trim();
                    final String psw = password_edt.getText().toString().trim();
                    progressDialog.show();
                    PackageInfo pInfo = null;
                    String version = null;
                    try {
                        pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                        version = pInfo.versionName;
                    } catch (PackageManager.NameNotFoundException e) {
                        e.printStackTrace();
                    }
                    final String finalVersion = version;

                    Log.d("LOGINURL:", AppUrls.BASE_URL + AppUrls.LOGIN);
                    StringRequest strReqLogin = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.LOGIN, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("LOGIN", response);
                                String editSuccessResponceCode = jsonObject.getString("status");

                                if (editSuccessResponceCode.equalsIgnoreCase("10100")) {

                                    progressDialog.dismiss();
                                    JSONObject jdata = jsonObject.getJSONObject("data");
                                    String jwt = jdata.getString("jwt");
                                    Log.d("JWT", jwt);
                                    String[] parts = jwt.split("\\.");
                                    byte[] dataDec = Base64.decode(parts[1], Base64.DEFAULT);
                                    String decodedString = "";
                                    try {

                                        decodedString = new String(dataDec, "UTF-8");
                                        Log.d("TOKENSFSF", decodedString);
                                        JSONObject jsonObject2 = new JSONObject(decodedString);
                                        Log.d("JSONDATAsfa", jsonObject2.toString());
                                        JSONObject jsonObject3 = jsonObject2.getJSONObject("data");

                                        String user_id = jsonObject3.getString("user_id");
                                        String user_name = jsonObject3.getString("user_name");
                                        String email = jsonObject3.getString("email");
                                        String mobile = jsonObject3.getString("mobile");
                                        final String account_status = jsonObject3.getString("account_status");
                                        String browser_session_id = jsonObject3.getString("browser_session_id");
                                        String address = jsonObject3.getString("address");
                                        String country = jsonObject3.getString("country");
                                        String state = jsonObject3.getString("state");
                                        String city = jsonObject3.getString("city");
                                        String area = jsonObject3.getString("area");
                                        String pincode = jsonObject3.getString("pincode");

                                        Log.d("LOGINUSERDETAIL:", jwt + "//" + user_id + "//" + user_name + "//" + email + "//" + mobile + "//" + account_status + "//" + browser_session_id);


                                        session.createUserLoginSession(jwt, user_id, user_name, mobile, email, account_status, browser_session_id, address, country, state, city, area, pincode);
//                                        Toast.makeText(getApplicationContext(), "Your Account has been Successfully verified...!", Toast.LENGTH_SHORT).show();
                                        //if (getIntent().getExtras().getString("activity_name").equals("splash")) {
                                        Log.v("activity_name", "" + getIntent().getExtras().getString("activity_name"));

                                        if (getIntent().getExtras().getString("activity_name").equals("splash")) {
                                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            startActivity(intent);
                                            finish();
                                            if (account_status.equals("0")) {
                                                Intent intse = new Intent(LoginActivity.this, MainActivity.class);
                                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                startActivity(intse);
                                                finish();
                                            }
                                        } else {
                                            finish();
                                        }
                                    } catch (UnsupportedEncodingException e) {
                                        e.printStackTrace();
                                    }

                                    Toast.makeText(getApplicationContext(), "You are Logged in Successfully...!", Toast.LENGTH_SHORT).show();
                                   /* Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);*/
                                }

                                if (editSuccessResponceCode.equalsIgnoreCase("10200")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "Your account has been disabled...!", Toast.LENGTH_SHORT).show();
                                }
                                if (editSuccessResponceCode.equalsIgnoreCase("10300")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "Login failed. Invalid username or password..!", Toast.LENGTH_SHORT).show();
                                }

                                if (editSuccessResponceCode.equalsIgnoreCase("11786")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "All fields are required..!", Toast.LENGTH_SHORT).show();
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {


                        @Override
                        protected Map<String, String> getParams() {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("email", email);
                            params.put("password", psw);
                            params.put("browser_id", device_id);
                            Log.d("LoginREQUESTDATA:", params.toString());
                            return params;
                        }
                       /* @Override
                        public byte[] getBody() throws AuthFailureError
                        {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("email", email);
                            params.put("password", psw);
                            Log.d("LoginREQUESTDATA ", "PARAMS " + params.toString());
                            return new JSONObject(params).toString().getBytes();
                        }*/
                    };

                    strReqLogin.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);
                    requestQueue.add(strReqLogin);


                } else {
                    Snackbar snackbar = Snackbar.make(view, "No Internet Connection...!", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
            }

        }
//        view == register_here_txt  // removed from bottom
        if (view == textSignIn) {
            Intent regIntent = new Intent(LoginActivity.this, RegisterActivity.class);
            regIntent.putExtra("activity_name", getIntent().getExtras().getString("activity_name"));
            startActivity(regIntent);
            finish();
            overridePendingTransition(R.anim.from_right, R.anim.out_left);
        }
        if (view == forgot_txt) {
            Intent regIntent = new Intent(LoginActivity.this, ForgetPasswordActivity.class);
            regIntent.putExtra("activity_name", getIntent().getExtras().getString("activity_name"));
            startActivity(regIntent);
            finish();
        }

    }

    private boolean validate() {

        boolean result = true;

        String EMAIL_REGEX = "^[\\w_\\.+]*[\\w_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        String username = username_edt.getText().toString().trim();
        String psw = password_edt.getText().toString().trim();

        if (username.length() == 0) {
            userNameTil.setError("Please enter Email");
            result = false;
        } else if (!username.matches(EMAIL_REGEX)) {
//            Toast.makeText(this, "Invalid Email", Toast.LENGTH_SHORT).show();
            userNameTil.setError("Invalid Email");
            result = false;
        } else if (psw.isEmpty() || psw.length() < 6) {
//            Toast.makeText(this, "Invalid Password. Password Must contain minimum 6 Characters", Toast.LENGTH_SHORT).show();
            //  userNameTil.setErrorEnabled(false);
            passwordTil.setError("Invalid Password. Password Must contain minimum 6 Characters");
            result = false;
        } else if (!username.matches(EMAIL_REGEX) && psw.isEmpty() || psw.length() < 6) {
            //  passwordTil.setErrorEnabled(false);
            Toast.makeText(this, "Invalid Email and Password", Toast.LENGTH_SHORT).show();
            userNameTil.setError("Invalid Email and Password");
            result = false;
        }


        return result;
    }
}
