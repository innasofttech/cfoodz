package innasoft.in.cfoodz.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import innasoft.in.cfoodz.MainActivity;
import innasoft.in.cfoodz.R;
import innasoft.in.cfoodz.utilities.AppUrls;

public class SuccessActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView close;
    TextView toolbar_tittle, success_txt, order_info;
    Typeface typeface;
    ProgressDialog progressDialog;
    String order_pid,razorpayPaymentID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_success);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Success");

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);
        progressDialog.setCancelable(false);

        typeface = Typeface.createFromAsset(this.getAssets(), "museosanscyrl.ttf");

        order_pid = getIntent().getExtras().getString("order_pid");
//        razorpayPaymentID = getIntent().getExtras().getString("razorpayPaymentID");

        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);
        toolbar_tittle = (TextView) findViewById(R.id.toolbar_tittle);
        order_info = (TextView) findViewById(R.id.order_info);
        toolbar_tittle.setTypeface(typeface);
        order_info.setTypeface(typeface);
        success_txt = (TextView) findViewById(R.id.success_txt);
        success_txt.setTypeface(typeface);

        order_info.setText(getIntent().getExtras().getString("message"));


        new Handler().postDelayed(new Runnable() {


            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                Intent i = new Intent(SuccessActivity.this, OrdersListActivity.class);
                startActivity(i);
                finish();
            }
        }, 5000);


//        paymentSuccess();
    }

    private void paymentSuccess() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.RAZORPAYSUCCESS+"order_id="+order_pid+"&transaction_id="+razorpayPaymentID,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("RESENDOTPRESP:", response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String successResponceCode = jsonObject.getString("status");
                            if (successResponceCode.equals("10100")) {

                                final String message = jsonObject.getString("message");

                                progressDialog.dismiss();
                                order_info.setText(message);

                                SuccessActivity.this.runOnUiThread(new Runnable() {
                                    public void run() {
                                        order_info.setText(message);
                                    }
                                });

                                /*Toast.makeText(SuccessActivity.this, "Payment Success", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(SuccessActivity.this, MainActivity.class);
                                startActivity(intent);*/

                            }
                            if (successResponceCode.equals("10200")) {

                            }

                            if (successResponceCode.equals("10300")) {

                            }
                            if (successResponceCode.equals("11786")) {
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(SuccessActivity.this);
        requestQueue.add(stringRequest);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onClick(View v) {
        if (v == close) {
            Intent i = new Intent(SuccessActivity.this, MainActivity.class);
            startActivity(i);
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(SuccessActivity.this, MainActivity.class);
        startActivity(i);
        finish();
    }
}
