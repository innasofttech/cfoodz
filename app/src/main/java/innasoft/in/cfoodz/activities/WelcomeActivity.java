package innasoft.in.cfoodz.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import innasoft.in.cfoodz.R;
import innasoft.in.cfoodz.utilities.UserSessionManager;


public class WelcomeActivity extends AppCompatActivity implements View.OnClickListener {

    TextView guest_btn, signin_btn, textView;
    Typeface typeface;

    UserSessionManager userSessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.poppins));
        userSessionManager = new UserSessionManager(WelcomeActivity.this);

        textView = findViewById(R.id.text);
        guest_btn = findViewById(R.id.guest_btn);
        signin_btn = findViewById(R.id.signin_btn);

        textView.setTypeface(typeface);
        guest_btn.setTypeface(typeface);
        signin_btn.setTypeface(typeface);


        guest_btn.setOnClickListener(this);
        signin_btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        if (v == guest_btn) {
            userSessionManager.createGuestLogin(true);
            Intent intent = new Intent(WelcomeActivity.this, ChooseLocComActivity.class);
            intent.putExtra("activity_name", "splash");
            startActivity(intent);
        }

        if (v == signin_btn) {

            Intent intent = new Intent(WelcomeActivity.this, LoginActivity.class);
            intent.putExtra("activity_name", "splash");
            startActivity(intent);

        }

    }
}
