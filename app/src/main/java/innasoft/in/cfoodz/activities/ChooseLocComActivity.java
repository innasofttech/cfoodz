package innasoft.in.cfoodz.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import innasoft.in.cfoodz.MainActivity;
import innasoft.in.cfoodz.R;
import innasoft.in.cfoodz.adapter.CommunityPlaceAutocompleteAdapter;
import innasoft.in.cfoodz.adapter.PlaceAutocompleteAdapter;
import innasoft.in.cfoodz.utilities.AppUrls;
import innasoft.in.cfoodz.utilities.CustomMapFragment;
import innasoft.in.cfoodz.utilities.GPSTracker;
import innasoft.in.cfoodz.utilities.MapWrapperLayout;
import innasoft.in.cfoodz.utilities.NetworkChecking;
import innasoft.in.cfoodz.utilities.UserSessionManager;

public class ChooseLocComActivity extends AppCompatActivity implements
        View.OnClickListener, MapWrapperLayout.OnDragListener,
        GoogleApiClient.OnConnectionFailedListener {

    Button buttonSubmit;
    String send_area_id = "", country = "", state = "", city = "", district = "";
    String pinCode = "";
    String token, user_id;

    Typeface typeface;

    private GoogleMap googleMap;
    private CustomMapFragment mCustomMapFragment;

    private View mMarkerParentView;
    private ImageView mMarkerImageView;

    private int imageParentWidth = -1;
    private int imageParentHeight = -1;
    private int imageHeight = -1;
    private int centerX = -1;
    private int centerY = -1;

    private TextView mLocationTextView;
    private AutoCompleteTextView mAutocompleteView;
    //    communitySearch;
    PlaceAutocompleteAdapter mAdapter;
    //    CommunityPlaceAutocompleteAdapter cAdapter;
    private GoogleApiClient mGoogleApiClient;
    private double mLatitude = 0, mLongitude = 0;
    //    , cLat = 0, cLng = 0;
    GPSTracker gpsTracker;
    boolean checkInternet;
    String activity_name;
    public static final String mypreference = "cfoodz_guest";
    SharedPreferences sharedpreferences, sp;
    UserSessionManager userSessionManager;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_loc_com);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Select Address");

        if (getIntent().getExtras() != null)
            activity_name = getIntent().getExtras().getString("activity_name");

        userSessionManager = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        user_id = userDetails.get(UserSessionManager.USER_ID);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.fonttype_one));
        gpsTracker = new GPSTracker(getApplicationContext());

        buttonSubmit = (Button) findViewById(R.id.buttonSubmit);
        mCustomMapFragment = ((CustomMapFragment) getFragmentManager().findFragmentById(R.id.map));
        mLocationTextView = (TextView) findViewById(R.id.location_text_view);
        mMarkerParentView = findViewById(R.id.marker_view_incl);
        mMarkerImageView = (ImageView) findViewById(R.id.marker_icon_view);
        mAutocompleteView = (AutoCompleteTextView) findViewById(R.id.googleplacesearch);
//        communitySearch = (AutoCompleteTextView) findViewById(R.id.communitySearch);


        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, 0 /* clientId */, this)
                .addApi(Places.GEO_DATA_API)
                .build();
        mAdapter = new PlaceAutocompleteAdapter(this, R.layout.google_places_search_items, mGoogleApiClient, null, null);
//        cAdapter = new CommunityPlaceAutocompleteAdapter(this, R.layout.google_places_search_items, mGoogleApiClient, null, null);

        mAutocompleteView.setAdapter(mAdapter);
        mAutocompleteView.setOnItemClickListener(mAutocompleteClickListener);

//        communitySearch.setAdapter(cAdapter);
//        communitySearch.setOnItemClickListener(mAutocompleteClickListener1);

        sp = getSharedPreferences(mypreference, Context.MODE_PRIVATE);
        if (sp.getString("address", "").length() > 0) {
//            communitySearch.setText(sp.getString("community_address", ""));
//            cLat = Double.parseDouble(sp.getString("community_lat", ""));
//            cLng = Double.parseDouble(sp.getString("community_long", ""));

            mLocationTextView.setText(sp.getString("address", ""));
            mAutocompleteView.setText(sp.getString("address", ""));
            mLatitude = Double.parseDouble(sp.getString("latitude", ""));
            mLongitude = Double.parseDouble(sp.getString("longitude", ""));
            country = sp.getString("country", "");
            state = sp.getString("state", "");
            city = sp.getString("city", "");
            district = sp.getString("district", "");
            send_area_id = sp.getString("area", "");
            pinCode = sp.getString("pincode", "");
        }


        mCustomMapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap gMap) {
                googleMap = gMap;

                if (ActivityCompat.checkSelfPermission(ChooseLocComActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                        ActivityCompat.checkSelfPermission(ChooseLocComActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                    return;
                }
                googleMap.setMyLocationEnabled(true);

                if (mLatitude == 0) {
                    LatLng currentLocation = new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude());
                    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 15f));
                    updateLocation(currentLocation);
                } else {
                    LatLng currentLocation = new LatLng(mLatitude, mLongitude);
                    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 15f));
                    updateLocation(currentLocation);
                }

                mCustomMapFragment.setOnDragListener(ChooseLocComActivity.this);

            }
        });


        buttonSubmit.setOnClickListener(this);
    }

    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {

        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                places.release();
                return;
            }

            final Place place = places.get(0);

            Log.v("name ", "" + places.get(0).getName());
            Log.v("getLatLng ", "" + places.get(0).getLatLng());
//            Log.v("name ", "" + places.get(0).getName());

            CharSequence attributions = places.getAttributions();
            if (attributions != null) {
//                mAttTextView.setText(Html.fromHtml(attributions.toString()));
                Log.v("attributes", "" + attributions.toString());
            }

            hideKeyboard();
            mLatitude = (place.getLatLng().latitude);
            mLongitude = (place.getLatLng().longitude);
            LatLng newLatLngTemp = new LatLng(place.getLatLng().latitude, place.getLatLng().longitude);
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(newLatLngTemp, 20f));
            updateLocation(newLatLngTemp);
        }
    };

/*    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback2
            = new ResultCallback<PlaceBuffer>() {

        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                places.release();
                return;
            }

            final Place place = places.get(0);
            hideKeyboard();
            cLat = (place.getLatLng().latitude);
            cLng = (place.getLatLng().longitude);
        }
    };*/

    public void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    private AdapterView.OnItemClickListener mAutocompleteClickListener
            = new AdapterView.OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            final PlaceAutocompleteAdapter.PlaceAutocomplete item1 = mAdapter.getItem(position);
            String placeId1 = "";
            if (item1 != null) {
                placeId1 = String.valueOf(item1.placeId);
            }
            Log.i("onclick ", "Selected: " + item1.description);
            PendingResult<PlaceBuffer> placeResult1 = Places.GeoDataApi
                    .getPlaceById(mGoogleApiClient, placeId1);
            placeResult1.setResultCallback(mUpdatePlaceDetailsCallback);

        }
    };

    /*private AdapterView.OnItemClickListener mAutocompleteClickListener1
            = new AdapterView.OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


            final CommunityPlaceAutocompleteAdapter.PlaceAutocomplete item2 = cAdapter.getItem(position);
            final String placeId2 = String.valueOf(item2.placeId);

            PendingResult<PlaceBuffer> placeResult2 = Places.GeoDataApi
                    .getPlaceById(mGoogleApiClient, placeId2);
            placeResult2.setResultCallback(mUpdatePlaceDetailsCallback2);

        }
    };*/

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);

        imageParentWidth = mMarkerParentView.getWidth();
        imageParentHeight = mMarkerParentView.getHeight();
        imageHeight = mMarkerImageView.getHeight();

        centerX = imageParentWidth / 2;
        centerY = (imageParentHeight / 2) + (imageHeight / 2);
    }

    @Override
    public void onClick(View v) {

        if (v == buttonSubmit) {

            checkInternet = NetworkChecking.isConnected(this);
            if (checkInternet) {
                if (!mLocationTextView.getText().toString().equals("") ||
                        mLocationTextView.getText() != null) {
//                        communitySearch.getText() != null ||
//                        !communitySearch.getText().toString().equals("")) {
                    Log.v("isGuest ", "" + userSessionManager.isGuest());
                    if (userSessionManager.isGuest()) {

                        sharedpreferences = getSharedPreferences(mypreference, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedpreferences.edit();
//                        editor.putString("community_address", communitySearch.getText().toString().trim());
//                        editor.putString("community_lat", String.valueOf(cLat));
//                        editor.putString("community_long", String.valueOf(cLng));
//                        editor.putString("address", mLocationTextView.getText().toString().trim());
//                        editor.putString("locality_lat", String.valueOf(mLatitude));
//                        editor.putString("locality_long", String.valueOf(mLongitude));
                        editor.putString("address", mLocationTextView.getText().toString().trim());
                        editor.putString("latitude", String.valueOf(mLatitude));
                        editor.putString("longitude", String.valueOf(mLongitude));
                        editor.putString("country", country);
                        editor.putString("state", state);
                        editor.putString("city", city);
                        editor.putString("district", district);
                        editor.putString("area", send_area_id);
                        editor.putString("pincode", pinCode);
                        editor.apply();

                        Intent intent = new Intent(ChooseLocComActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();

                    } else {
                        progressDialog.show();
                        sendLocToServer();
                    }
                } else {
                    Toast.makeText(ChooseLocComActivity.this, "Please Choose your Location or Community.", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(this, "Check Your Internet Connection.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void sendLocToServer() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.SENDLOCTOSERVER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String successResponceCode = jsonObject.getString("status");
                            if (successResponceCode.equals("10100")) {

                                userSessionManager.updateUserDetails(mLocationTextView.getText().toString().trim(), country, state, city, send_area_id, pinCode);

                                if (getIntent().getExtras().getString("activity_name").equalsIgnoreCase("splash")) {
                                    Intent intent = new Intent(ChooseLocComActivity.this, MainActivity.class);
                                    startActivity(intent);
                                    finish();
                                } else {
                                    finish();
                                }
                            }
                            if (successResponceCode.equals("10200")) {
                                Toast.makeText(getApplicationContext(), "Sorry, Try again...!", Toast.LENGTH_SHORT).show();
                            }

                            if (successResponceCode.equals("10300")) {
                                Toast.makeText(getApplicationContext(), "Incorrect Mobile No....!", Toast.LENGTH_SHORT).show();
                            }
                            if (successResponceCode.equals("11786")) {
                                Toast.makeText(getApplicationContext(), "All fields are required...!", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                } else if (error instanceof AuthFailureError) {

                } else if (error instanceof ServerError) {

                } else if (error instanceof NetworkError) {

                } else if (error instanceof ParseError) {

                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
//                params.put("community_address", communitySearch.getText().toString().trim());
//                params.put("community_lat", String.valueOf(cLat));
//                params.put("community_long", String.valueOf(cLng));

                params.put("user_id", user_id);
                params.put("address", mLocationTextView.getText().toString().trim());
                params.put("latitude", String.valueOf(mLatitude));
                params.put("longitude", String.valueOf(mLongitude));
                params.put("country", country);
                params.put("state", state);
                params.put("city", city);
                params.put("district", district);
                params.put("area", send_area_id);
                params.put("pincode", pinCode);

                Log.d("PARAMDATA:", params.toString());
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(ChooseLocComActivity.this);
        requestQueue.add(stringRequest);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onDrag(MotionEvent motionEvent) {
        if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
            Projection projection = (googleMap != null && googleMap
                    .getProjection() != null) ? googleMap.getProjection()
                    : null;
            if (projection != null) {
                LatLng centerLatLng = projection.fromScreenLocation(new Point(
                        centerX, centerY));
                updateLocation(centerLatLng);
            }
        }
    }

    private void updateLocation(LatLng centerLatLng) {
        if (centerLatLng != null) {
            Geocoder geocoder = new Geocoder(ChooseLocComActivity.this,
                    Locale.getDefault());

            List<Address> addresses = new ArrayList<Address>();
            try {
                addresses = geocoder.getFromLocation(centerLatLng.latitude,
                        centerLatLng.longitude, 1);
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (addresses != null && addresses.size() > 0) {

                String addressIndex0 = (addresses.get(0).getAddressLine(0) != null) ? addresses
                        .get(0).getAddressLine(0) : null;

                String completeAddress = addressIndex0;

                if (completeAddress != null) {
                    mLocationTextView.setText(completeAddress);
                    pinCode = addresses.get(0).getPostalCode();
                    mLatitude = centerLatLng.latitude;
                    mLongitude = centerLatLng.longitude;

                    send_area_id = addresses.get(0).getSubLocality();
                    ;
                    city = addresses.get(0).getLocality();
                    state = addresses.get(0).getAdminArea();
                    country = addresses.get(0).getCountryName();
                    district = addresses.get(0).getSubAdminArea();
                }
            }
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
