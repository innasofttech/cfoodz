package innasoft.in.cfoodz.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.provider.Settings;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.transition.Slide;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.transition.Visibility;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import innasoft.in.cfoodz.MainActivity;
import innasoft.in.cfoodz.R;
import innasoft.in.cfoodz.utilities.AppUrls;
import innasoft.in.cfoodz.utilities.NetworkChecking;
import innasoft.in.cfoodz.utilities.UserSessionManager;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    TextView login_here_txt, textSignIn;
    EditText name_edt, email_edt, mobile_edt, password_edt, confrm_password_edt;
    TextInputLayout name_til, email_til, mobile_til, password_til, confrm_password_til;
    Button register_btn;
    private boolean checkInternet;
    ProgressDialog progressDialog;
    UserSessionManager session;
    AppBarLayout app_bar_layout;
    Typeface typeface;
    String device_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        device_id = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);

        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.fonttype_one));

        app_bar_layout = (AppBarLayout) findViewById(R.id.app_bar_layout);
        session = new UserSessionManager(getApplicationContext());

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Registering......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        name_til = (TextInputLayout) findViewById(R.id.name_til);
        email_til = (TextInputLayout) findViewById(R.id.email_til);
        mobile_til = (TextInputLayout) findViewById(R.id.mobile_til);
        password_til = (TextInputLayout) findViewById(R.id.password_til);
        confrm_password_til = (TextInputLayout) findViewById(R.id.confrm_password_til);
        textSignIn = findViewById(R.id.textSignIn);
        textSignIn.setTypeface(typeface);

        String text = "<font color='#b5b5b5'>Sign In </font>  <font color='#ffffff'><b>| Sign Up</b></font>";
        textSignIn.setText(Html.fromHtml(text), TextView.BufferType.SPANNABLE);
        textSignIn.setOnClickListener(this);

        name_edt = (EditText) findViewById(R.id.name_edt);
        name_edt.setTypeface(typeface);
      /*  name_edt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(final View v, boolean hasFocus) {
                if (hasFocus) {
                    app_bar_layout.setExpanded(false, true);
                }
            }
        });*/
        email_edt = (EditText) findViewById(R.id.email_edt);
        email_edt.setTypeface(typeface);
      /*  email_edt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(final View v, boolean hasFocus) {
                if (hasFocus) {
                    app_bar_layout.setExpanded(false, true);
                }
            }
        });*/
        mobile_edt = (EditText) findViewById(R.id.mobile_edt);
        mobile_edt.setTypeface(typeface);
     /*   mobile_edt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(final View v, boolean hasFocus) {
                if (hasFocus) {
                    app_bar_layout.setExpanded(false, true);
                }
            }
        });*/
        password_edt = (EditText) findViewById(R.id.password_edt);
        password_edt.setTypeface(typeface);
      /*  password_edt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(final View v, boolean hasFocus) {
                if (hasFocus) {
                    app_bar_layout.setExpanded(false, true);
                }
            }
        });*/
        confrm_password_edt = (EditText) findViewById(R.id.confrm_password_edt);
        confrm_password_edt.setTypeface(typeface);
       /* confrm_password_edt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(final View v, boolean hasFocus) {
                if (hasFocus) {
                    app_bar_layout.setExpanded(false, true);
                }
            }
        });*/

        login_here_txt = (TextView) findViewById(R.id.login_here_txt);
        login_here_txt.setTypeface(typeface);
        login_here_txt.setOnClickListener(this);

        register_btn = (Button) findViewById(R.id.register_btn);
        register_btn.setTypeface(typeface);
        register_btn.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        if (view == register_btn) {
            if (validate()) {
                checkInternet = NetworkChecking.isConnected(this);

                if (checkInternet) {
                    final String name = name_edt.getText().toString().trim();
                    final String mobile = mobile_edt.getText().toString().trim();
                    final String email = email_edt.getText().toString().trim();
                    final String password = password_edt.getText().toString().trim();

                    progressDialog.show();
                    PackageInfo pInfo = null;
                    String version = null;
                    try {
                        pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                        version = pInfo.versionName;
                    } catch (PackageManager.NameNotFoundException e) {
                        e.printStackTrace();
                    }
                    final String finalVersion = version;

                    Log.d("REGURL", AppUrls.BASE_URL + AppUrls.REGISTRATION);
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.REGISTRATION,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    progressDialog.dismiss();
                                    Log.d("RESPONCELREG", response);
                                    try {

                                        JSONObject jsonObject = new JSONObject(response);
                                        String editSuccessResponceCode = jsonObject.getString("status");
                                        if (editSuccessResponceCode.equalsIgnoreCase("10100")) {
                                            // JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                            progressDialog.dismiss();

//                                            loginApi();

                                            Log.v("activity_name", "" + getIntent().getExtras().getString("activity_name"));

                                            Toast.makeText(getApplicationContext(), "Send OTP to your mobile....! ", Toast.LENGTH_SHORT).show();
                                            session.createGuestLogin(false);
                                            Intent intent = new Intent(RegisterActivity.this, AccountVerificationActivity.class);
                                            intent.putExtra("activity_name", getIntent().getExtras().getString("activity_name"));
                                            intent.putExtra("mobile", mobile);
                                            startActivity(intent);
                                            finish();
                                        }
                                        if (editSuccessResponceCode.equals("10400")) {
                                            Toast.makeText(getApplicationContext(), "Email already exits", Toast.LENGTH_SHORT).show();
                                        }
                                        if (editSuccessResponceCode.equals("10300")) {
                                            Toast.makeText(getApplicationContext(), "Mobile already exits", Toast.LENGTH_SHORT).show();
                                        }
                                        if (editSuccessResponceCode.equals("10200")) {
                                            Toast.makeText(getApplicationContext(), "Sorry, Try again....!", Toast.LENGTH_SHORT).show();
                                        }
                                        if (editSuccessResponceCode.equals("11786")) {
                                            Toast.makeText(getApplicationContext(), "All fields are required....!", Toast.LENGTH_SHORT).show();
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    progressDialog.dismiss();

                                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                    } else if (error instanceof AuthFailureError) {

                                    } else if (error instanceof ServerError) {

                                    } else if (error instanceof NetworkError) {

                                    } else if (error instanceof ParseError) {

                                    }
                                }
                            }) {

                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("name", name);
                            params.put("email", email);
                            params.put("mobile", mobile);
                            params.put("conf_pwd", password);
                            Log.d("RegisterREQUESTDATA:", params.toString());
                            return params;
                        }
                      /* @Override
                       public byte[] getBody() throws AuthFailureError
                       {
                           Map<String, String> params = new HashMap<String, String>();
                           params.put("name", name);
                           params.put("email", email);
                           params.put("mobile", mobile);
                           params.put("conf_pwd", password);

                           Log.d("RegisterREQUESTDATA ", params.toString());
                           return new JSONObject(params).toString().getBytes();
                       }*/

                    };

                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    RequestQueue requestQueue = Volley.newRequestQueue(RegisterActivity.this);
                    requestQueue.add(stringRequest);


                } else {
                    Snackbar snackbar = Snackbar.make(view, "No Internet Connection...!", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
            }
        }

//        view == login_here_txt // removed from bottom

        if (view == textSignIn) {
            Intent ilogin = new Intent(RegisterActivity.this, LoginActivity.class);
            ilogin.putExtra("activity_name", getIntent().getExtras().getString("activity_name"));
            startActivity(ilogin);
            finish();
            overridePendingTransition(R.anim.from_left, R.anim.out_right);
        }
    }

    private boolean validate() {

        boolean result = true;
        int flag = 0;

        String EMAIL_REGEX = "^[\\w_\\.+]*[\\w_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        String MOBILE_REGEX = "^[789]\\d{9}$";

        // if (!name.matches("[a-zA-Z_]+")) { System.out.println("Invalid name"); }

        String name = name_edt.getText().toString().trim();
        if ((name == null || name.equals("") || name.length() < 3)) {
            name_til.setError("Minimum 3 characters required");
            result = false;
        } else {
            if (!name.matches("^[\\p{L} .'-]+$")) {
                name_til.setError("Special characters not allowed");
                result = false;

            } else {
                name_til.setErrorEnabled(false);
            }
        }


        String mobile = mobile_edt.getText().toString().trim();
        if ((mobile == null || mobile.equals("")) || mobile.length() != 10 || !mobile.matches(MOBILE_REGEX)) {
            mobile_til.setError("Invalid Mobile Number");
            result = false;
        } else {
            mobile_til.setErrorEnabled(false);
        }

        String email = email_edt.getText().toString().trim();
        if (!email.matches(EMAIL_REGEX)) {
            email_til.setError("Invalid Email");
            result = false;
        } else {
            email_til.setErrorEnabled(false);
        }


        String password = password_edt.getText().toString().trim();
        if (password.isEmpty() || password.length() < 6) {
            password_til.setError("Minimum 6 characters required");
            result = false;
        } else {
            password_til.setErrorEnabled(false);
        }

        String confrm_password = confrm_password_edt.getText().toString().trim();
        if (confrm_password.isEmpty() || password.length() < 6) {
            confrm_password_til.setError("Minimum 6 characters required");
            result = false;
        } else {
            confrm_password_til.setErrorEnabled(false);
        }

        if (password != "" && confrm_password != "" && !confrm_password.equals(password) && flag == 0) {
            //password_edt.setError("Password and Confirm not Match");
            confrm_password_til.setError("Password and Confirm Password not Match");
            result = false;
        } else {
            confrm_password_til.setErrorEnabled(false);
        }


        return result;
    }

}
