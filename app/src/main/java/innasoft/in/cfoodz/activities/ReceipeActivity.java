package innasoft.in.cfoodz.activities;

import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bluejamesbond.text.DocumentView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import innasoft.in.cfoodz.R;
import innasoft.in.cfoodz.models.RecepieModel;
import innasoft.in.cfoodz.utilities.AppUrls;
import innasoft.in.cfoodz.utilities.NetworkChecking;

public class ReceipeActivity extends AppCompatActivity implements View.OnClickListener{

    String loc_id,product_id,recep_id;
    ProgressDialog progressDialog;
    Typeface typeface,typeface2;
    private boolean checkInternet;
    TextView language_text_title;
    DocumentView content_text;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receipe);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Recipe Detail");
        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.fonttype_one));
        typeface2 = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.fonttype_two));

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);


        Bundle bundle = getIntent().getExtras();
        loc_id = bundle.getString("location_id");
        product_id = bundle.getString("product_id");
        recep_id = bundle.getString("recipe_id");

        Log.d("GETDETAIL",loc_id+"//"+product_id+"//"+recep_id);
        language_text_title=(TextView)findViewById(R.id.language_text_title);
        language_text_title.setTypeface(typeface2);

        content_text=(DocumentView)findViewById(R.id.content_text);


        getDetailContent();
    }

    private void getDetailContent()
    {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet)
        {
            Log.d("URLRECEDETAIL", AppUrls.BASE_URL + AppUrls.PRODUCT_RECEIPE_SINGLEVIEW);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.PRODUCT_RECEIPE_SINGLEVIEW,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try
                            {
                                Log.d("RecepRECEDETAIL",response);
                                JSONObject jsonObject = new JSONObject(response);
                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100"))
                                {
                                    JSONObject jjobdata = jsonObject.getJSONObject("data");
                                    JSONObject jOBJ=jjobdata.getJSONObject("recordData");
                                    Log.d("JAARARARA",jOBJ.toString());

                                    progressDialog.cancel();

                                    String id=jOBJ.getString("id");
                                    String language_name=jOBJ.getString("language_name");
                                    String content=jOBJ.getString("content");

                                    language_text_title.setText(language_name);

                                    content_text.setText(Html.fromHtml(content));

                                }

                                if (responceCode.equals("12786"))
                                {
                                    progressDialog.cancel();
                                    Toast.makeText(ReceipeActivity.this, "No Data Found...!", Toast.LENGTH_SHORT).show();

                                }
                                if (responceCode.equals("11786"))
                                {
                                    progressDialog.cancel();
                                    Toast.makeText(ReceipeActivity.this, "All fields are required..!", Toast.LENGTH_SHORT).show();

                                }
                                if (responceCode.equals("10786"))
                                {
                                    progressDialog.cancel();
                                    Toast.makeText(ReceipeActivity.this, "Invalid Token..!", Toast.LENGTH_SHORT).show();

                                }

                            } catch (JSONException e) {
                                progressDialog.cancel();
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            })
            {
/*

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Authorization-Basic", token);
                    Log.d("ORDERCANCELHEADER",headers.toString());
                    return headers;
                }
*/

                @Override
                protected Map<String, String> getParams() throws AuthFailureError
                {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("location_id",loc_id);
                    params.put("product_id",product_id);
                    params.put("recipe_id",recep_id);
                    Log.d("RECEDETAILPPARAM",params.toString());
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(ReceipeActivity.this);
            requestQueue.add(stringRequest);

        } else {
            Snackbar snackbar = Snackbar.make(getWindow().getDecorView().getRootView(), "No Internet Connection...!", Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }

    @Override
    public void onClick(View view)
    {

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
