package innasoft.in.cfoodz.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import innasoft.in.cfoodz.R;
import innasoft.in.cfoodz.adapter.ChildcategoryAdapter;
import innasoft.in.cfoodz.adapter.MainCategoryProductsAdapter;
import innasoft.in.cfoodz.adapter.SubcategoryAdapter;
import innasoft.in.cfoodz.models.MostSellingModel;
import innasoft.in.cfoodz.models.SubcategoryModel;
import innasoft.in.cfoodz.utilities.AppUrls;
import innasoft.in.cfoodz.utilities.CitySelectionSession;
import innasoft.in.cfoodz.utilities.NetworkChecking;

public class MainCategoryProductsActivity extends AppCompatActivity implements View.OnClickListener {

    private boolean checkInternet;
    public String banner_mage;
    RecyclerView most_selling_recyclerview;
    ArrayList<MostSellingModel> mostsellinglist;
    MainCategoryProductsAdapter mostsellingadapter;
    CitySelectionSession citySelectionSession;
    public String disp_city_name = "", disp_city_id="";
    int defaultPageNo = 1;
    private static int displayedposition = 0;
    private static int displayedposition1 = 0;
    int total_number_of_items = 0;
    private boolean loading = true;
    int type_of_request = 0;
    private boolean userScrolled = true;
    GridLayoutManager  layoutManager;
    LinearLayoutManager  layoutManager_linear;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    String sort_by;
    private Boolean exit = false;
    private TextView bottomSheetHeading;
    LinearLayout sort_layout, filter_layout,main_layout;
    BottomSheetBehavior behavior;
    TextView relevance,price_low_to_high,price_high_to_low,newest,popularity,most_selling,sort_filter_type;
    String weight_id = "empty",min_price = "empty",max_price = "empty";
    String sub_category_id = "0",child_category_id = "0";
    View bottomSheet;
    TextView subcategory_txt,child_category_txt;
    ProgressDialog progressDialog;
    AlertDialog subcategorydialog;
    AlertDialog Childcategorydialog;
    ImageView image_grid,subscribe_img;
    /*Countrys List*/
    SubcategoryAdapter subcategoryAdapter;
    ChildcategoryAdapter childcategoryAdapter;
    ArrayList<SubcategoryModel> subcategoryModels = new ArrayList<SubcategoryModel>();
    ArrayList<SubcategoryModel> child_cat_models = new ArrayList<SubcategoryModel>();
    ArrayList<String> subcategoryList = new ArrayList<String>();
    ArrayList<String> childcategoryList = new ArrayList<String>();
    String send_subcategory_id = "";
    String child_avil_id = "";
    String child_cat_id = "";
    private boolean inBed = false;
    FloatingActionButton fab_btn;
    boolean clicked = true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_category_products);

        citySelectionSession = new CitySelectionSession(MainCategoryProductsActivity.this);
        HashMap<String, String> cityDetails = citySelectionSession.getCityDetails();
            disp_city_id = cityDetails.get(CitySelectionSession.CITY_ID);
            disp_city_name = cityDetails.get(CitySelectionSession.CITY_NAME);

            //   disp_lng = cityDetails.get(CitySelectionSession.CITY_LAT);
            //   disp_lng = cityDetails.get(CitySelectionSession.CITY_LNG);
            try{


            }catch (NullPointerException e){
                String msg = (e.getMessage()==null)?"Login failed!":e.getMessage();
                Log.i("Login Error1",msg);
            }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        if (getIntent().getExtras().getString("cat_id").equals("1")){
            getSupportActionBar().setTitle("Fish");
        } else if (getIntent().getExtras().getString("cat_id").equals("2")){
            getSupportActionBar().setTitle("Prawn");
        }else if (getIntent().getExtras().getString("cat_id").equals("3")){
            getSupportActionBar().setTitle("Crab");
        }

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        subcategory_txt = (TextView) findViewById(R.id.subcategory_txt);
        child_category_txt = (TextView) findViewById(R.id.child_category_txt);
        sort_filter_type = (TextView) findViewById(R.id.sort_filter_type);
        subcategory_txt.setOnClickListener(this);
        child_category_txt.setOnClickListener(this);


        subscribe_img = findViewById(R.id.subscribe_img);
        subscribe_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse("tel:" + Uri.encode("08106670757")));
                callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(callIntent);
            }
        });

        sort_layout = (LinearLayout)findViewById(R.id.sort_layout);
        filter_layout = (LinearLayout)findViewById(R.id.filter_layout);
        main_layout = (LinearLayout)findViewById(R.id.main_layout);
        relevance = (TextView) findViewById(R.id.relevance);
        price_low_to_high = (TextView) findViewById(R.id.price_low_to_high);
        price_high_to_low = (TextView) findViewById(R.id.price_high_to_low);
        newest = (TextView) findViewById(R.id.newest);
        popularity = (TextView) findViewById(R.id.popularity);
        most_selling = (TextView) findViewById(R.id.most_selling);
        fab_btn = (FloatingActionButton) findViewById(R.id.fab_btn);
        image_grid = (ImageView) findViewById(R.id.image_grid);

       /* fab_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!clicked){
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        fab_btn.setImageDrawable(getResources().getDrawable(R.drawable.list_icon, getApplicationContext().getTheme()));
                    } else {
                        fab_btn.setImageDrawable(getResources().getDrawable(R.drawable.list_icon));
                    }
                    clicked = true;


                    most_selling_recyclerview.setHasFixedSize(true);
                    mostsellingadapter = new MainCategoryProductsAdapter(mostsellinglist, MainCategoryProductsActivity.this, R.layout.main_category_products_row);
                    layoutManager =  new GridLayoutManager(MainCategoryProductsActivity.this,2);
                    most_selling_recyclerview.setAdapter(mostsellingadapter);
                    most_selling_recyclerview.setLayoutManager(layoutManager);
                    most_selling_recyclerview.setItemAnimator(new DefaultItemAnimator());

                }else{
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        fab_btn.setImageDrawable(getResources().getDrawable(R.drawable.grid_icon, getApplicationContext().getTheme()));
                    } else {
                        fab_btn.setImageDrawable(getResources().getDrawable(R.drawable.grid_icon));
                    }
                    clicked = false;
                    most_selling_recyclerview.setHasFixedSize(true);
                    mostsellingadapter = new MainCategoryProductsAdapter(mostsellinglist, MainCategoryProductsActivity.this, R.layout.main_category_products_row_linear);
                    layoutManager_linear = new LinearLayoutManager(MainCategoryProductsActivity.this);
                    most_selling_recyclerview.setLayoutManager(layoutManager_linear);
                    most_selling_recyclerview.setAdapter(mostsellingadapter);
                    most_selling_recyclerview.setItemAnimator(new DefaultItemAnimator());

                }
            }
        });*/
        /*image_grid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!clicked){
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        image_grid.setBackground( getResources().getDrawable(R.drawable.list_icon));
                       // image_grid.setImageDrawable(getResources().getDrawable(R.drawable.list_icon, getApplicationContext().getTheme()));
                    } else {
                        image_grid.setBackground( getResources().getDrawable(R.drawable.list_icon));
                    }
                    clicked = true;


                    most_selling_recyclerview.setHasFixedSize(true);
                    mostsellingadapter = new MainCategoryProductsAdapter(mostsellinglist, MainCategoryProductsActivity.this, R.layout.main_category_products_row);
                    layoutManager =  new GridLayoutManager(MainCategoryProductsActivity.this,2);
                    most_selling_recyclerview.setAdapter(mostsellingadapter);
                    most_selling_recyclerview.setLayoutManager(layoutManager);
                    most_selling_recyclerview.setItemAnimator(new DefaultItemAnimator());

                }else{
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                       // image_grid.setImageDrawable(getResources().getDrawable(R.drawable.grid_icon, getApplicationContext().getTheme()));
                        image_grid.setBackground( getResources().getDrawable(R.drawable.grid_icon));
                    } else {
                        //image_grid.setImageDrawable(getResources().getDrawable(R.drawable.grid_icon));
                        image_grid.setBackground( getResources().getDrawable(R.drawable.grid_icon));
                    }
                    clicked = false;
                    most_selling_recyclerview.setHasFixedSize(true);
                    mostsellingadapter = new MainCategoryProductsAdapter(mostsellinglist, MainCategoryProductsActivity.this, R.layout.main_category_products_row_linear);
                    layoutManager_linear = new LinearLayoutManager(MainCategoryProductsActivity.this);
                    most_selling_recyclerview.setLayoutManager(layoutManager_linear);
                    most_selling_recyclerview.setAdapter(mostsellingadapter);
                    most_selling_recyclerview.setItemAnimator(new DefaultItemAnimator());

                }
            }
        });*/

        most_selling_recyclerview = (RecyclerView)findViewById(R.id.most_selling_recyclerview);
        mostsellinglist = new ArrayList<MostSellingModel>();
        mostsellingadapter = new MainCategoryProductsAdapter(mostsellinglist, MainCategoryProductsActivity.this, R.layout.main_category_products_row);
        layoutManager =  new GridLayoutManager(MainCategoryProductsActivity.this,1);
    //    RecyclerView.LayoutManager layoutManager1 = new GridLayoutManager(MainCategoryProductsActivity.this,2);
        most_selling_recyclerview.setNestedScrollingEnabled(false);
        most_selling_recyclerview.setLayoutManager(layoutManager);
        sort_by = "id-desc";
        sort_filter_type.setText(" - Relevance");
        getMostSellingProducts(defaultPageNo,sort_by,weight_id,min_price,max_price,sub_category_id,child_category_id);
        most_selling_recyclerview.addOnScrollListener(new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    userScrolled = true;

                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy)
            {
                if(dy > 0)
                {

                    try {
                        visibleItemCount = layoutManager.getChildCount();
                        totalItemCount = layoutManager.getItemCount();
                        pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();
                        visibleItemCount = layoutManager_linear.getChildCount();
                        totalItemCount = layoutManager_linear.getItemCount();
                        pastVisiblesItems = layoutManager_linear.findFirstVisibleItemPosition();
                    }catch (Exception e){

                    }
                    displayedposition = pastVisiblesItems;
                    displayedposition1 = pastVisiblesItems;
                    Log.v("SDFASFAFAFAF", "Last Item Wow !"+pastVisiblesItems+", "+visibleItemCount+", "+totalItemCount);

                    if (loading)
                    {
                        if ( userScrolled && (visibleItemCount + pastVisiblesItems) >= totalItemCount)
                        {
                            loading = false;
                            userScrolled = false;
                            Log.v("SDFASFAFAFAF", "Last Item Wow !"+pastVisiblesItems+", "+visibleItemCount+", "+totalItemCount);
                            defaultPageNo = defaultPageNo + 1;
                            Log.d("PPPPPPPPP", String.valueOf(defaultPageNo));
                            if(totalItemCount < total_number_of_items) {
                                type_of_request = 1;

                                getMostSellingProducts(defaultPageNo,sort_by,weight_id,min_price,max_price,sub_category_id,child_category_id);

                            }else {
                                Toast.makeText(MainCategoryProductsActivity.this, "No More Results", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
            }
        });
        bottomSheet = findViewById(R.id.design_bottom_sheet);

        behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        most_selling_recyclerview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        });
        bottomSheet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        });
        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_DRAGGING:
                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_DRAGGING");
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_SETTLING");
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_EXPANDED");
                        break;
                    case BottomSheetBehavior.STATE_COLLAPSED:
                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_COLLAPSED");
                        break;
                    case BottomSheetBehavior.STATE_HIDDEN:
                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_HIDDEN");
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                Log.i("BottomSheetCallback", "slideOffset: " + slideOffset);
            }
        });
        filter_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              Intent intent = new Intent(MainCategoryProductsActivity.this,DummyFilterActivity.class);
                intent.putExtra("cat_id",getIntent().getExtras().getString("cat_id"));
                intent.putExtra("sub_cat_id",sub_category_id);
                intent.putExtra("child_cat_id",child_category_id);
                startActivityForResult(intent, 2);
            }
        });
        sort_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        relevance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                sort_by = "id-desc";
                sort_filter_type.setText(" - Relevance");
                displayedposition = 0;
                displayedposition1 = 0;
                defaultPageNo = 1;
                mostsellinglist.clear();
                getMostSellingProducts(defaultPageNo,sort_by,weight_id,min_price,max_price,sub_category_id,child_category_id);
            }
        });
        price_low_to_high.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                sort_by = "price-asc";
                sort_filter_type.setText(" - Low to High");
                displayedposition = 0;
                displayedposition1 = 0;
                defaultPageNo = 1;
                mostsellinglist.clear();
                getMostSellingProducts(defaultPageNo,sort_by,weight_id,min_price,max_price,sub_category_id,child_category_id);
            }
        });
        price_high_to_low.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                sort_by = "price-desc";
                sort_filter_type.setText(" - High to Low");
                displayedposition = 0;
                displayedposition1 = 0;
                defaultPageNo = 1;
                mostsellinglist.clear();
                getMostSellingProducts(defaultPageNo,sort_by,weight_id,min_price,max_price,sub_category_id,child_category_id);
            }
        });
        newest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                sort_by = "features-1";
                sort_filter_type.setText(" - Newest");
                displayedposition = 0;
                displayedposition1 = 0;
                defaultPageNo = 1;
                mostsellinglist.clear();
                getMostSellingProducts(defaultPageNo,sort_by,weight_id,min_price,max_price,sub_category_id,child_category_id);
            }
        });
        popularity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                sort_by = "features-2";
                sort_filter_type.setText(" - Popularity");
                displayedposition = 0;
                displayedposition1 = 0;
                defaultPageNo = 1;
                mostsellinglist.clear();
                getMostSellingProducts(defaultPageNo,sort_by,weight_id,min_price,max_price,sub_category_id,child_category_id);
            }
        });
        most_selling.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                sort_by = "features-3";
                sort_filter_type.setText(" - Most Selling");
                displayedposition = 0;
                displayedposition1 = 0;
                defaultPageNo = 1;
                mostsellinglist.clear();
                getMostSellingProducts(defaultPageNo,sort_by,weight_id,min_price,max_price,sub_category_id,child_category_id);
            }
        });

    }

    private void getMostSellingProducts(final int default_page_number, final String sort_by, final String wei_id, final String mi_price, final String ma_price, final String sub_cat_id, final String child_cat_id) {



        checkInternet = NetworkChecking.isConnected(MainCategoryProductsActivity.this);
        if (checkInternet) {

            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.GET_REFINE_FILTER_PRODUCTS,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("ressdjfasdh1233",response);
                                String responceCode = jsonObject.getString("status");
                                if(responceCode.equals("10100")) {
                                    most_selling_recyclerview.setVisibility(View.VISIBLE);

                                    JSONObject json = jsonObject.getJSONObject("data");
                                    JSONArray jsonArray = json.getJSONArray("recordData");
                                    int jsonArray_count = json.getInt("recordTotalCnt");

                                    total_number_of_items = jsonArray_count;
                                    Log.d("LOADSTATUS",  "OUTER "+loading+"  "+jsonArray_count);
                                    if(jsonArray_count > mostsellinglist.size())
                                    {
                                        loading = true;
                                        Log.d("LOADSTATUS",  "INNER "+loading+"  "+jsonArray_count);
                                    }else {
                                        loading = false;
                                    }

                                    for (int i = 0; i < jsonArray.length(); i++) {

                                        MostSellingModel gbm = new MostSellingModel();

                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        gbm.setId(jsonObject1.getString("id"));
                                        gbm.setName(jsonObject1.getString("product_name"));
                                        gbm.setUrl_name(jsonObject1.getString("url_name"));
                                        gbm.setCount_id(jsonObject1.getString("location_id"));
                                        gbm.setWeight_id(jsonObject1.getString("weight_id"));
                                        gbm.setCount_name(jsonObject1.getString("count_name"));
                                        gbm.setWeight_name(jsonObject1.getString("weight_name"));
                                        gbm.setQty(jsonObject1.getString("qty"));
                                        gbm.setMrp_price(jsonObject1.getString("mrp_price"));
                                        gbm.setImages(AppUrls.PRODUCTS_IMAGE_URL+jsonObject1.getString("images"));
                                        gbm.setFeatures(jsonObject1.getString("features"));
                                        gbm.setStatus(jsonObject1.getString("status"));
                                        gbm.setUser_rating(jsonObject1.getString("user_rating"));



                                        mostsellinglist.add(gbm);

                                    }

                                    try {
                                        layoutManager.scrollToPositionWithOffset(displayedposition , mostsellinglist.size());
                                        layoutManager_linear.scrollToPositionWithOffset(displayedposition1, mostsellinglist.size());
                                    }catch (Exception e){

                                    }
                                    most_selling_recyclerview.setAdapter(mostsellingadapter);

                                }
                                if (responceCode.equals("12786")){
                                    Toast.makeText(MainCategoryProductsActivity.this, "No Data Found..!", Toast.LENGTH_SHORT).show();
                                    most_selling_recyclerview.setVisibility(View.GONE);
                                }
                                if(responceCode.equals("11786"))
                                {
                                    Toast.makeText(MainCategoryProductsActivity.this, "Error..!", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {


                    if (error instanceof TimeoutError || error instanceof NoConnectionError)
                    {
                    } else if (error instanceof AuthFailureError)
                    {
                    } else if (error instanceof ServerError)
                    {
                    } else if (error instanceof NetworkError)
                    {
                    } else if (error instanceof ParseError)
                    {
                    }
                }
            }){


                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("location_id", disp_city_id);
                    params.put("page_no", String.valueOf(default_page_number));
                    params.put("main_category_id", getIntent().getExtras().getString("cat_id"));
                    params.put("sub_category_id", sub_cat_id);
                    params.put("child_category_id", child_cat_id);
                    params.put("sort_by", sort_by);
                    params.put("weight_id", wei_id);
                    params.put("min_price", mi_price);
                    params.put("max_price", ma_price);
                    Log.d("LoginREQUESTDATA:",params.toString());
                    return params;
                }
                       /* @Override
                        public byte[] getBody() throws AuthFailureError
                        {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("email", email);
                            params.put("password", psw);
                            Log.d("LoginREQUESTDATA ", "PARAMS " + params.toString());
                            return new JSONObject(params).toString().getBytes();
                        }*/
            };

            RequestQueue requestQueue = Volley.newRequestQueue(MainCategoryProductsActivity.this);
            requestQueue.add(stringRequest);

        }else {
            Toast.makeText(this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if(requestCode==2)
        {mostsellinglist.clear();
            defaultPageNo = 1;
            displayedposition = 0;
            displayedposition1 = 0;
            String mi_price=data.getStringExtra("min_price");
            min_price = mi_price;
            String ma_price=data.getStringExtra("max_price");
            max_price = ma_price;
            String weights=data.getStringExtra("weights");
            weight_id = weights;

            String sub_cat=data.getStringExtra("sub_cat");
            sub_category_id = sub_cat;

            String child_cat=data.getStringExtra("child_cat");
            child_category_id = child_cat;
            getMostSellingProducts(defaultPageNo,sort_by,weight_id,min_price,max_price,sub_category_id,child_category_id);
        }
    }

    @Override
    public void onClick(View v) {
        if (v == subcategory_txt){
            subcategoryList();
        }if (v == child_category_txt){
            childCategoryList();
        }
    }

    private void subcategoryList() {
        subcategoryModels.clear();
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            progressDialog.show();
            Log.d("SubCategory",AppUrls.BASE_URL + AppUrls.GET_SUBCATEGORY);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.GET_SUBCATEGORY,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("SubCategoryResponce",response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")){

                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
//                                    String recordTotalCnt = jsonObject1.getString("recordTotalCnt");

                                    JSONArray jsonArray = jsonObject1.getJSONArray("recordData");
                                    progressDialog.cancel();

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        SubcategoryModel cm = new SubcategoryModel();
                                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                        cm.setId(jsonObject2.getString("id"));
                                        String subcategory_name = jsonObject2.getString("name");
                                        String child_available = jsonObject2.getString("child_available");
                                        cm.setName(subcategory_name);
                                        cm.setChild_available(child_available);

                                        subcategoryList.add(subcategory_name);
                                        subcategoryModels.add(cm);
                                    }
                                    subcategoryDialog();
                                }

                                if (responceCode.equals("12786")){
                                    Toast.makeText(MainCategoryProductsActivity.this, "No Subcategories Found..!", Toast.LENGTH_SHORT).show();
                                }

                                if (responceCode.equals("10786")){
                                    Toast.makeText(MainCategoryProductsActivity.this, "Invalid Token..!", Toast.LENGTH_SHORT).show();
                                }
                                if(responceCode.equals("11786"))
                                {
                                    Toast.makeText(MainCategoryProductsActivity.this, "All Fields Required..!", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                progressDialog.cancel();
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();

                    if (error instanceof TimeoutError || error instanceof NoConnectionError)
                    {
                    } else if (error instanceof AuthFailureError)
                    {
                    } else if (error instanceof ServerError)
                    {
                    } else if (error instanceof NetworkError)
                    {
                    } else if (error instanceof ParseError)
                    {
                    }
                }
            })
            {

                @Override
                protected Map<String, String> getParams() throws AuthFailureError
                {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("main_category_id", getIntent().getExtras().getString("cat_id"));
                    params.put("page_no", "1");
                    Log.d("SubcategoryParams:",params.toString());
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);

        }else {
            Snackbar snackbar = Snackbar.make(getWindow().getDecorView().getRootView(), "No Internet Connection...!", Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }

    private void subcategoryDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();

        View dialog_layout = inflater.inflate(R.layout.subcategory_list,null);

        TextView subcategory_txt = (TextView) dialog_layout.findViewById(R.id.subcategory_txt);

        final SearchView subcategory_search = (SearchView) dialog_layout.findViewById(R.id.subcategory_search);
        EditText searchEditText = (EditText) subcategory_search.findViewById(android.support.v7.appcompat.R.id.search_src_text);

        RecyclerView subcategory_recyclerview = (RecyclerView) dialog_layout.findViewById(R.id.subcategory_recyclerview);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        subcategory_recyclerview.setLayoutManager(layoutManager);

        subcategoryAdapter = new SubcategoryAdapter(subcategoryModels, MainCategoryProductsActivity.this,R.layout.row_subcategory);

        subcategory_recyclerview.setAdapter(subcategoryAdapter);
        subcategory_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                subcategory_search.setIconified(false);
            }
        });

        builder.setView(dialog_layout).setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                progressDialog.dismiss();
                dialogInterface.dismiss();
            }
        });

        subcategorydialog = builder.create();

        subcategory_search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                subcategoryAdapter.getFilter().filter(query);
                return false;
            }
        });
        subcategorydialog.show();
    }

    public void setSubcategoryName(String subcategoryName, String subcategoryId, String child_available) {

        subcategorydialog.dismiss();
        subcategoryAdapter.notifyDataSetChanged();

        String sendSubcategoryName = subcategoryName;
        subcategory_txt.setText(sendSubcategoryName);
        send_subcategory_id = subcategoryId;
        sub_category_id = send_subcategory_id;
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        displayedposition = 0;
        displayedposition1 = 0;
        defaultPageNo = 1;
        child_avil_id = child_available;
        if (!child_avil_id.equals("0")){
            child_category_txt.setVisibility(View.VISIBLE);
           // childCategoryList();
        }
        else {
            child_category_txt.setVisibility(View.GONE);
        }
        mostsellinglist.clear();
        getMostSellingProducts(defaultPageNo,sort_by,weight_id,min_price,max_price,sub_category_id,child_category_id);
    }
    private void childCategoryList() {
        child_cat_models.clear();
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            progressDialog.show();
            Log.d("SubCategory",AppUrls.BASE_URL + AppUrls.GET_SUBCATEGORY);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.GET_CHILD_CATEGORY,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("SubCategoryResponce",response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")){

                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
//                                    String recordTotalCnt = jsonObject1.getString("recordTotalCnt");

                                    JSONArray jsonArray = jsonObject1.getJSONArray("recordData");
                                    progressDialog.cancel();

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        SubcategoryModel cm = new SubcategoryModel();
                                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                        cm.setId(jsonObject2.getString("id"));
                                        String subcategory_name = jsonObject2.getString("name");

                                        cm.setName(subcategory_name);


                                        childcategoryList.add(subcategory_name);
                                        child_cat_models.add(cm);
                                    }
                                    childCategoryDialog();
                                }

                                if (responceCode.equals("12786")){
                                    Toast.makeText(MainCategoryProductsActivity.this, "No Subcategories Found..!", Toast.LENGTH_SHORT).show();
                                }

                                if (responceCode.equals("10786")){
                                    Toast.makeText(MainCategoryProductsActivity.this, "Invalid Token..!", Toast.LENGTH_SHORT).show();
                                }
                                if(responceCode.equals("11786"))
                                {
                                    Toast.makeText(MainCategoryProductsActivity.this, "All Fields Required..!", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                progressDialog.cancel();
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();

                    if (error instanceof TimeoutError || error instanceof NoConnectionError)
                    {
                    } else if (error instanceof AuthFailureError)
                    {
                    } else if (error instanceof ServerError)
                    {
                    } else if (error instanceof NetworkError)
                    {
                    } else if (error instanceof ParseError)
                    {
                    }
                }
            })
            {

                @Override
                protected Map<String, String> getParams() throws AuthFailureError
                {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sub_category_id", sub_category_id);
                   // params.put("page_no", "1");
                    Log.d("SubcategoryParams:",params.toString());
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);

        }else {
            Snackbar snackbar = Snackbar.make(getWindow().getDecorView().getRootView(), "No Internet Connection...!", Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }
    private void childCategoryDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();

        View dialog_layout = inflater.inflate(R.layout.subcategory_list,null);

        TextView subcategory_txt = (TextView) dialog_layout.findViewById(R.id.subcategory_txt);

        final SearchView subcategory_search = (SearchView) dialog_layout.findViewById(R.id.subcategory_search);
        EditText searchEditText = (EditText) subcategory_search.findViewById(android.support.v7.appcompat.R.id.search_src_text);

        RecyclerView subcategory_recyclerview = (RecyclerView) dialog_layout.findViewById(R.id.subcategory_recyclerview);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        subcategory_recyclerview.setLayoutManager(layoutManager);

        childcategoryAdapter = new ChildcategoryAdapter(child_cat_models, MainCategoryProductsActivity.this,R.layout.row_subcategory);

        subcategory_recyclerview.setAdapter(childcategoryAdapter);
        subcategory_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                subcategory_search.setIconified(false);
            }
        });

        builder.setView(dialog_layout).setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                progressDialog.dismiss();
                dialogInterface.dismiss();
            }
        });

        Childcategorydialog = builder.create();

        subcategory_search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                subcategoryAdapter.getFilter().filter(query);
                return false;
            }
        });
        Childcategorydialog.show();
    }
    public void setChildcategoryName(String subcategoryName, String subcategoryId) {

        Childcategorydialog.dismiss();
        subcategoryAdapter.notifyDataSetChanged();

        String sendSubcategoryName = subcategoryName;
        child_category_txt.setText(sendSubcategoryName);
        child_category_id = subcategoryId;

        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        displayedposition = 0;
        displayedposition1 = 0;
        defaultPageNo = 1;

        mostsellinglist.clear();
        getMostSellingProducts(defaultPageNo,sort_by,weight_id,min_price,max_price,sub_category_id,child_category_id);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
