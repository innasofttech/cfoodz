package innasoft.in.cfoodz.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


import innasoft.in.cfoodz.R;
import innasoft.in.cfoodz.itemclicklistners.MostSellingItemClickListener;

public class MostSellingHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    public TextView name,rating;
    public TextView price,count_no;
    public ImageView image;
    public ImageView new_image;
//            ,like_image;


    MostSellingItemClickListener mostSellingItemClickListener;

    public MostSellingHolder(View itemView) {
        super(itemView);

        itemView.setOnClickListener(this);

        name = (TextView) itemView.findViewById(R.id.name);
        rating = (TextView) itemView.findViewById(R.id.rating);
        price = (TextView) itemView.findViewById(R.id.price);
        count_no = (TextView) itemView.findViewById(R.id.count_no);
        image = (ImageView) itemView.findViewById(R.id.image);
        new_image = (ImageView) itemView.findViewById(R.id.new_image);
//        like_image = (ImageView) itemView.findViewById(R.id.like_image);
    }

    @Override
    public void onClick(View view) {

        this.mostSellingItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(MostSellingItemClickListener ic)
    {
        this.mostSellingItemClickListener=ic;
    }
}
