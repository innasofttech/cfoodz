package innasoft.in.cfoodz.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import innasoft.in.cfoodz.R;
import innasoft.in.cfoodz.itemclicklistners.BlogListItemClickListener;
import innasoft.in.cfoodz.itemclicklistners.ReceipeListItemClickListener;

public class ReceipeListHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    public TextView receipe_lang_text,receipe_content;
    public Button receipe_lang_button_more;


    ReceipeListItemClickListener blogListItemClickListener;

    public ReceipeListHolder(View itemView) {
        super(itemView);

        itemView.setOnClickListener(this);

        receipe_lang_text = (TextView) itemView.findViewById(R.id.receipe_lang_text);
        receipe_content = (TextView) itemView.findViewById(R.id.receipe_content);
        receipe_lang_button_more = (Button) itemView.findViewById(R.id.receipe_lang_button_more);

    }

    @Override
    public void onClick(View view) {

        this.blogListItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(ReceipeListItemClickListener ic)
    {
        this.blogListItemClickListener=ic;
    }
}
