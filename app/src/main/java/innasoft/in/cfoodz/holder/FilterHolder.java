package innasoft.in.cfoodz.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import innasoft.in.cfoodz.R;
import innasoft.in.cfoodz.itemclicklistners.MostSellingItemClickListener;

public class FilterHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    public CheckBox check_box;


    MostSellingItemClickListener mostSellingItemClickListener;

    public FilterHolder(View itemView) {
        super(itemView);

        itemView.setOnClickListener(this);

        check_box = (CheckBox) itemView.findViewById(R.id.check_box);

    }

    @Override
    public void onClick(View view) {

        this.mostSellingItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(MostSellingItemClickListener ic)
    {
        this.mostSellingItemClickListener=ic;
    }
}
