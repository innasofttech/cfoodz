package innasoft.in.cfoodz.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import innasoft.in.cfoodz.R;
import innasoft.in.cfoodz.itemclicklistners.BlogListItemClickListener;
import innasoft.in.cfoodz.itemclicklistners.MostSellingItemClickListener;

public class BlogListHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    public TextView blog_title_text;
    public ImageView blog_image_iv;

    BlogListItemClickListener blogListItemClickListener;

    public BlogListHolder(View itemView) {
        super(itemView);

        itemView.setOnClickListener(this);

        blog_title_text = (TextView) itemView.findViewById(R.id.blog_title_text);
        blog_image_iv = (ImageView) itemView.findViewById(R.id.blog_image_iv);
    }

    @Override
    public void onClick(View view) {

        this.blogListItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(BlogListItemClickListener ic)
    {
        this.blogListItemClickListener=ic;
    }
}
