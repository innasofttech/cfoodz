package innasoft.in.cfoodz.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import innasoft.in.cfoodz.R;
import innasoft.in.cfoodz.itemclicklistners.SubcategoryItemClickListener;

public class ChildcategoryHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    public TextView subcategory_name_txt;
    SubcategoryItemClickListener subcategoryItemClickListener;

    public ChildcategoryHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        subcategory_name_txt = (TextView) itemView.findViewById(R.id.subcategory_name_txt);
    }

    @Override
    public void onClick(View view) {
        this.subcategoryItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(SubcategoryItemClickListener ic)
    {
        this.subcategoryItemClickListener = ic;
    }
}
