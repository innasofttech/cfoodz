package innasoft.in.cfoodz.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import innasoft.in.cfoodz.R;
import innasoft.in.cfoodz.itemclicklistners.BlogListItemClickListener;
import innasoft.in.cfoodz.itemclicklistners.CartListItemClickListener;

public class CartListHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    public TextView cart_product_name,cart_product_skuid,cart_product_count,cart_product_prize,cart_product_qty,total_item;
    public ImageView cart_product_img,qty_minus_img,qty_add_img;
    public ImageView remove_btn;

    CartListItemClickListener cartListItemClickListener;

    public CartListHolder(View itemView) {
        super(itemView);

        itemView.setOnClickListener(this);

        cart_product_name = (TextView) itemView.findViewById(R.id.cart_product_name);
        cart_product_skuid = (TextView) itemView.findViewById(R.id.cart_product_skuid);
        cart_product_count = (TextView) itemView.findViewById(R.id.cart_product_count);
        cart_product_prize = (TextView) itemView.findViewById(R.id.cart_product_prize);
        cart_product_qty = (TextView) itemView.findViewById(R.id.cart_product_qty);
        total_item = (TextView) itemView.findViewById(R.id.total_item);

        cart_product_img = (ImageView) itemView.findViewById(R.id.cart_product_img);

        qty_minus_img = (ImageView) itemView.findViewById(R.id.qty_minus_img);
        qty_add_img = (ImageView) itemView.findViewById(R.id.qty_add_img);

        remove_btn = (ImageView) itemView.findViewById(R.id.remove_btn);

    }

    @Override
    public void onClick(View view) {

        this.cartListItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(CartListItemClickListener ic)
    {
        this.cartListItemClickListener=ic;
    }
}
