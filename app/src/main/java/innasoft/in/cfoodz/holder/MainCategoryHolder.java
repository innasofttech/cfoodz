package innasoft.in.cfoodz.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import innasoft.in.cfoodz.R;
import innasoft.in.cfoodz.itemclicklistners.MainCategoryItemClickListener;
import innasoft.in.cfoodz.itemclicklistners.WishListItemClickListener;

public class MainCategoryHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    public TextView category_text;
    public ImageView category_img;



    MainCategoryItemClickListener mainCategoryItemClickListener;

    public MainCategoryHolder(View itemView) {
        super(itemView);

        itemView.setOnClickListener(this);

        category_text = (TextView) itemView.findViewById(R.id.category_text);
        category_img = (ImageView) itemView.findViewById(R.id.category_img);

    }

    @Override
    public void onClick(View view) {

        this.mainCategoryItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(MainCategoryItemClickListener ic)
    {
        this.mainCategoryItemClickListener=ic;
    }
}
