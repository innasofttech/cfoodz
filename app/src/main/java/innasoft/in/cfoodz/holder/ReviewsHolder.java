package innasoft.in.cfoodz.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import innasoft.in.cfoodz.R;
import innasoft.in.cfoodz.itemclicklistners.MostSellingItemClickListener;

public class ReviewsHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    public TextView name,review_text;
    public RatingBar rating_bar;


    MostSellingItemClickListener mostSellingItemClickListener;

    public ReviewsHolder(View itemView) {
        super(itemView);

        itemView.setOnClickListener(this);

        name = (TextView) itemView.findViewById(R.id.name);
        review_text = (TextView) itemView.findViewById(R.id.review_text);
        rating_bar = (RatingBar) itemView.findViewById(R.id.rating_bar);

    }

    @Override
    public void onClick(View view) {

        this.mostSellingItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(MostSellingItemClickListener ic)
    {
        this.mostSellingItemClickListener=ic;
    }
}
