package innasoft.in.cfoodz.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import innasoft.in.cfoodz.R;
import innasoft.in.cfoodz.itemclicklistners.ShippingItemClickListener;
import innasoft.in.cfoodz.itemclicklistners.ShippingPaymentItemClickListener;

public class ShippingPaymentHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    public RadioButton payment_order_id_txt,pay_gateway_name;
    public ImageView cod_logo;
    ShippingPaymentItemClickListener shippingPaymentItemClickListener;

    public ShippingPaymentHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);


        cod_logo = (ImageView) itemView.findViewById(R.id.cod_logo);
        pay_gateway_name = (RadioButton) itemView.findViewById(R.id.pay_gateway_name);

    }

    @Override
    public void onClick(View view) {
        this.shippingPaymentItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(ShippingPaymentItemClickListener ic)
    {
        this.shippingPaymentItemClickListener = ic;
    }
}
