package innasoft.in.cfoodz.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import innasoft.in.cfoodz.R;
import innasoft.in.cfoodz.itemclicklistners.SubcategoryItemClickListener;
import innasoft.in.cfoodz.itemclicklistners.TestimonialItemClickListener;

public class TestimonialHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    public TextView testimonial_tittle,testimonial_desp;
    TestimonialItemClickListener testimonialItemClickListener;

    public TestimonialHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        testimonial_tittle = (TextView) itemView.findViewById(R.id.testimonial_tittle);
        testimonial_desp = (TextView) itemView.findViewById(R.id.testimonial_desp);
    }

    @Override
    public void onClick(View view) {
        this.testimonialItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(TestimonialItemClickListener ic)
    {
        this.testimonialItemClickListener = ic;
    }
}
