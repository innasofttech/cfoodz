package innasoft.in.cfoodz.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.daimajia.slider.library.SliderLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import innasoft.in.cfoodz.R;
import innasoft.in.cfoodz.adapter.MainCategoryAdapter;
import innasoft.in.cfoodz.adapter.MostSellingAdapter;
import innasoft.in.cfoodz.adapter.NewProductAdapter;
import innasoft.in.cfoodz.adapter.PopularProductAdapter;
import innasoft.in.cfoodz.adapter.SliderPagerAdapter;
import innasoft.in.cfoodz.models.CityModel;
import innasoft.in.cfoodz.models.GetBannerModel;
import innasoft.in.cfoodz.models.MainCategoryModel;
import innasoft.in.cfoodz.models.MostSellingModel;
import innasoft.in.cfoodz.utilities.AppUrls;
import innasoft.in.cfoodz.utilities.CitySelectionSession;
import innasoft.in.cfoodz.utilities.NetworkChecking;
import innasoft.in.cfoodz.utilities.viewpager.EnchantedViewPager;


public class HomeFragment extends Fragment {

    View view;

    private boolean checkInternet;
    ArrayList<GetBannerModel> getbannermodel = new ArrayList<>();
    ArrayList<String> imgArray = new ArrayList<>();
    SliderLayout slider_image;
    SliderPagerAdapter mCutomSliderPagerAdapter;
    public String banner_mage;
    RecyclerView most_selling_recyclerview;
    RecyclerView new_products_recyclerview;
    RecyclerView papular_products_recyclerview;
    ArrayList<MostSellingModel> mostsellinglist;
    ArrayList<MostSellingModel> newproductlist;
    ArrayList<MostSellingModel> popularproductlist;
    MostSellingAdapter mostsellingadapter;
    NewProductAdapter newproductadapter;
    PopularProductAdapter popularproductadapter;
    LinearLayout fish_layout, prawn_layout, crab_layout;
    public String disp_city_name = "", disp_city_id = "";
    CitySelectionSession citySelectionSession;

    RecyclerView main_categ_recyclerView;
    ArrayList<MainCategoryModel> mainCategoryList = new ArrayList<MainCategoryModel>();
    MainCategoryAdapter mainCategoryAdapter;
    int defaultPageNo = 1;
    private static int displayedposition = 0;
    int total_number_of_items = 0;
    private boolean loading = true;
    int type_of_request = 0;
    private boolean userScrolled = true;
    LinearLayoutManager layoutManager;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    GridLayoutManager gridLayoutManagerVertical;
    private EnchantedViewPager viewPager;

    ArrayList<String> citi_list = new ArrayList<>();
    ArrayList<CityModel> homecityModels = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_home, container, false);


        slider_image = (SliderLayout) view.findViewById(R.id.slider_image);
        citySelectionSession = new CitySelectionSession(getActivity());

        loadCityData();

        most_selling_recyclerview = (RecyclerView) view.findViewById(R.id.most_selling_recyclerview);

        mostsellinglist = new ArrayList<MostSellingModel>();
        mostsellingadapter = new MostSellingAdapter(mostsellinglist, HomeFragment.this, R.layout.most_selling_row);
        most_selling_recyclerview.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        //  RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getContext(),2);
        most_selling_recyclerview.setNestedScrollingEnabled(false);
        // most_selling_recyclerview.setLayoutManager(layoutManager);
        new_products_recyclerview = (RecyclerView) view.findViewById(R.id.new_products_recyclerview);
        newproductlist = new ArrayList<MostSellingModel>();
        newproductadapter = new NewProductAdapter(newproductlist, HomeFragment.this, R.layout.new_products_row);
        new_products_recyclerview.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        RecyclerView.LayoutManager layoutManager1 = new GridLayoutManager(getContext(), 2);
        new_products_recyclerview.setNestedScrollingEnabled(false);
        StaggeredGridLayoutManager gridLayoutManager =
                new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
// Attach the layout manager to the recycler view
        gridLayoutManagerVertical =
                new GridLayoutManager(getContext(),
                        2, //The number of Columns in the grid
                        LinearLayoutManager.VERTICAL,
                        false);
        // gridLayoutManagerVertical.setSpanSizeLookup(new MySpanSizeLookup(5, 1, 2));
        gridLayoutManager.setReverseLayout(true);
        //   mLayoutManager.setStackFromEnd(true);
        new_products_recyclerview.setLayoutManager(gridLayoutManagerVertical);
        //   new_products_recyclerview.setLayoutManager(layoutManager1);
        papular_products_recyclerview = (RecyclerView) view.findViewById(R.id.papular_products_recyclerview);
        popularproductlist = new ArrayList<MostSellingModel>();
        popularproductadapter = new PopularProductAdapter(popularproductlist, HomeFragment.this, R.layout.most_selling_row);
        papular_products_recyclerview.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        // RecyclerView.LayoutManager layoutManager2 = new GridLayoutManager(getContext(),2);
        papular_products_recyclerview.setNestedScrollingEnabled(false);

        main_categ_recyclerView = (RecyclerView) view.findViewById(R.id.main_categ_recyclerView);

        mainCategoryAdapter = new MainCategoryAdapter(mainCategoryList, HomeFragment.this, R.layout.row_main_category);
        // main_categ_recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL, false));
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getContext(), 1);
        main_categ_recyclerView.setNestedScrollingEnabled(false);
        main_categ_recyclerView.setLayoutManager(layoutManager);

        return view;
    }

    private void getCityData() {
        checkInternet = NetworkChecking.isConnected(getActivity());
        if (checkInternet) {
            homecityModels.clear();
            StringRequest string_request = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.LOCATION,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {
//                            Log.e("RESPCITY", "RESPCITY:" + response.toString());
                            try {
                                JSONObject jsonObject = new JSONObject(response);
//                                Log.d("Citites:", response);
                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        CityModel pcm = new CityModel();
                                        pcm.setCityId(jsonObject1.getString("id"));
                                        String city_name = jsonObject1.getString("name");
                                        pcm.setCityName(city_name);
                                        citi_list.add(city_name);
                                        homecityModels.add(pcm);
                                    }
                                    //   cityDialog();
                                    citySelectionSession.createCity(homecityModels.get(0).getCityId(), homecityModels.get(0).getCityName());
                                    loadCityData();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getActivity(), "Error:" + error, Toast.LENGTH_LONG).show();
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    } else if (error instanceof AuthFailureError) {
                        //TODO
                    } else if (error instanceof ServerError) {
                        //TODO
                    } else if (error instanceof NetworkError) {

                    } else if (error instanceof ParseError) {
                        //TODO
                    }
                }
            });

            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(string_request);
        } else {
            Toast.makeText(getActivity(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void loadCityData() {
        if (!citySelectionSession.checkCityCreated()) {
            HashMap<String, String> cityDetails = citySelectionSession.getCityDetails();
            disp_city_id = cityDetails.get(CitySelectionSession.CITY_ID);
            disp_city_name = cityDetails.get(CitySelectionSession.CITY_NAME);

//            getSliders(disp_city_id);
            getMainCategory(defaultPageNo);

        } else {
            getCityData();
        }
    }

    private void getMainCategory(final int default_page_number) {
        checkInternet = NetworkChecking.isConnected(getActivity());
        if (checkInternet) {
            Log.d("MAINCATEGURL", AppUrls.BASE_URL + AppUrls.GET_MAIN_CATEGORY);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.GET_MAIN_CATEGORY,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("MAINCATEGRESP", response);
                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    JSONObject json = jsonObject.getJSONObject("data");
                                    int jsonArray_count = json.getInt("recordTotalCnt");
                                    JSONArray jsonArray = json.getJSONArray("recordData");
                                    total_number_of_items = jsonArray_count;
                                    Log.d("LOADSTATUS", "OUTER " + loading + "  " + jsonArray_count);
                                    if (jsonArray_count > mainCategoryList.size()) {
                                        loading = true;
                                        Log.d("LOADSTATUS", "INNER " + loading + "  " + jsonArray_count);
                                    } else {
                                        loading = false;
                                    }

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        MainCategoryModel gbm = new MainCategoryModel();
                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        gbm.setCategoryId(jsonObject1.getString("id"));
                                        gbm.setCategoryName(jsonObject1.getString("name"));
                                        mainCategoryList.add(gbm);
                                    }
                                    // layoutManager.scrollToPositionWithOffset(displayedposition , mainCategoryList.size());
                                    main_categ_recyclerView.setAdapter(mainCategoryAdapter);

                                }
                                if (responceCode.equals("12786")) {
                                    Toast.makeText(getActivity(), "No Data Found..!", Toast.LENGTH_SHORT).show();
                                }
                                if (responceCode.equals("11786")) {
                                    Toast.makeText(getActivity(), "All fields are required..!", Toast.LENGTH_SHORT).show();
                                }
                                if (responceCode.equals("10786")) {
                                    Toast.makeText(getActivity(), "Invalid Token.!", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {


                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("page_no", String.valueOf(default_page_number));
                    Log.d("MainCATEREQUESTDATA:", params.toString());
                    return params;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(stringRequest);

        } else {
            Toast.makeText(getActivity(), "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }

    }


    private void getSliders(final String loc_id) {


        checkInternet = NetworkChecking.isConnected(getActivity());
        if (checkInternet) {

            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.GET_BANNER,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);

                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {


                                    JSONArray jsonArray = jsonObject.getJSONArray("data");

                                    for (int i = 0; i < jsonArray.length(); i++) {

                                        GetBannerModel gbm = new GetBannerModel();

                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        String id = jsonObject1.getString("id");
                                        String image = jsonObject1.getString("image");
                                        imgArray.add(AppUrls.SLIDER_IMAGE_URL + image);
                                        banner_mage = AppUrls.SLIDER_IMAGE_URL + image;
                                        Log.d("IMAGEARRAYList:", imgArray.toString());


                                        gbm.setId(id);
                                        gbm.setImage(banner_mage);


                                        getbannermodel.add(gbm);


                                        viewPager = (EnchantedViewPager) view.findViewById(R.id.viewpager);
                                        viewPager.setClipToPadding(false);
                                        viewPager.useScale();
                                        viewPager.setPageMargin(0);
                                        mCutomSliderPagerAdapter = new SliderPagerAdapter(HomeFragment.this, getbannermodel);
                                        viewPager.setAdapter(mCutomSliderPagerAdapter);

                                        viewPager.setPageTransformer(false, new ViewPager.PageTransformer() {
                                            @Override
                                            public void transformPage(View page, float position) {
                                                int pageWidth = viewPager.getMeasuredWidth() - viewPager.getPaddingLeft() - viewPager.getPaddingRight();
                                                int pageHeight = viewPager.getHeight()+50;
                                                int paddingLeft = viewPager.getPaddingLeft();
                                                float transformPos = (float) (page.getLeft() - (viewPager.getScrollX() + paddingLeft)) / pageWidth;

                                                final float normalizedposition = Math.abs(Math.abs(transformPos) - 1);
                                                page.setAlpha(normalizedposition + 0.5f);

                                                int max = -pageHeight / 30;

                                                if (transformPos < -1) { // [-Infinity,-1)
                                                    // This page is way off-screen to the left.
                                                    page.setTranslationY(0);
                                                } else if (transformPos <= 1) { // [-1,1]
                                                    page.setTranslationY(max * (1 - Math.abs(transformPos)));

                                                } else { // (1,+Infinity]
                                                    // This page is way off-screen to the right.
                                                    page.setTranslationY(0);
                                                }


                                            }
                                        });

                                       /* HashMap<String,String> file_maps = new HashMap<String, String>();
                                        file_maps.put("Hannibal",banner_mage);
                                        for(String name : file_maps.keySet())
                                        {
                                            TextSliderView textSliderView = new TextSliderView(getContext());
                                            textSliderView
                                                    .description("")
                                                    .image(file_maps.get(name))
                                                    .setScaleType(BaseSliderView.ScaleType.Fit);
                                            textSliderView.bundle(new Bundle());
                                            slider_image.addSlider(textSliderView);
                                        }
                                        slider_image.setPresetTransformer(SliderLayout.Transformer.Default);
                                        slider_image.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
                                        slider_image.setDuration(3000);*/
                                    }
                                    getMostSellingProducts(loc_id);

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {


                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {


                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("unique_id", loc_id);
                    Log.d("LoginREQUESTDATA:", params.toString());
                    return params;
                }
                       /* @Override
                        public byte[] getBody() throws AuthFailureError
                        {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("email", email);
                            params.put("password", psw);
                            Log.d("LoginREQUESTDATA ", "PARAMS " + params.toString());
                            return new JSONObject(params).toString().getBytes();
                        }*/
            };

            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(stringRequest);

        } else {
            Snackbar snackbar = Snackbar.make(view, "No Internet Connection...!", Snackbar.LENGTH_LONG);
            snackbar.show();
        }

    }

    private void getMostSellingProducts(final String loc_id) {


        checkInternet = NetworkChecking.isConnected(getActivity());
        if (checkInternet) {

            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.GET_MOST_SELLING_PRODUCTLIST,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);

                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {


                                    JSONObject json = jsonObject.getJSONObject("data");
                                    JSONArray jsonArray = json.getJSONArray("recordData");

                                    for (int i = 0; i < jsonArray.length(); i++) {

                                        MostSellingModel gbm = new MostSellingModel();

                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        gbm.setId(jsonObject1.getString("id"));
                                        gbm.setName(jsonObject1.getString("name"));
                                        gbm.setUrl_name(jsonObject1.getString("url_name"));
                                        gbm.setCount_id(jsonObject1.getString("count_id"));
                                        gbm.setWeight_id(jsonObject1.getString("weight_id"));
                                        gbm.setCount_name(jsonObject1.getString("count_name"));
                                        gbm.setWeight_name(jsonObject1.getString("weight_name"));
                                        gbm.setQty(jsonObject1.getString("qty"));
                                        gbm.setMrp_price(jsonObject1.getString("mrp_price"));
                                        gbm.setImages(AppUrls.PRODUCTS_IMAGE_URL + jsonObject1.getString("images"));
                                        gbm.setFeatures(jsonObject1.getString("features"));
                                        gbm.setStatus(jsonObject1.getString("status"));
                                        gbm.setUser_rating(jsonObject1.getString("user_rating"));


                                        mostsellinglist.add(gbm);

                                    }

                                    most_selling_recyclerview.setAdapter(mostsellingadapter);
                                    getNewProducts(loc_id);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {


                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {


                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("location_id", loc_id);
                    params.put("page_no", "1");
                    Log.d("LoginREQUESTDATA:", params.toString());
                    return params;
                }
                       /* @Override
                        public byte[] getBody() throws AuthFailureError
                        {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("email", email);
                            params.put("password", psw);
                            Log.d("LoginREQUESTDATA ", "PARAMS " + params.toString());
                            return new JSONObject(params).toString().getBytes();
                        }*/
            };

            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(stringRequest);

        } else {
            Snackbar snackbar = Snackbar.make(view, "No Internet Connection...!", Snackbar.LENGTH_LONG);
            snackbar.show();
        }

    }

    private void getNewProducts(final String loc_id) {


        checkInternet = NetworkChecking.isConnected(getActivity());
        if (checkInternet) {

            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.GET_NEW_PRODUCTLIST,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("ksdnsdsdagsa", response);

                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {


                                    JSONObject json = jsonObject.getJSONObject("data");
                                    JSONArray jsonArray = json.getJSONArray("recordData");

                                    for (int i = 0; i < 4; i++) {

                                        MostSellingModel gbm = new MostSellingModel();

                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        gbm.setId(jsonObject1.getString("id"));
                                        gbm.setName(jsonObject1.getString("name"));
                                        gbm.setUrl_name(jsonObject1.getString("url_name"));
                                        gbm.setCount_id(jsonObject1.getString("count_id"));
                                        gbm.setWeight_id(jsonObject1.getString("weight_id"));
                                        gbm.setCount_name(jsonObject1.getString("count_name"));
                                        gbm.setWeight_name(jsonObject1.getString("weight_name"));
                                        gbm.setQty(jsonObject1.getString("qty"));
                                        gbm.setMrp_price(jsonObject1.getString("mrp_price"));
                                        gbm.setImages(AppUrls.PRODUCTS_IMAGE_URL + jsonObject1.getString("images"));
                                        gbm.setFeatures(jsonObject1.getString("features"));
                                        gbm.setStatus(jsonObject1.getString("status"));
                                        gbm.setUser_rating(jsonObject1.getString("user_rating"));


                                        newproductlist.add(gbm);

                                    }

                                    new_products_recyclerview.setAdapter(newproductadapter);
                                    getPopularProducts(loc_id);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {


                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {


                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("location_id", loc_id);
                    params.put("page_no", "1");
                    Log.d("LoginREQUESTDATA:", params.toString());
                    return params;
                }
                       /* @Override
                        public byte[] getBody() throws AuthFailureError
                        {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("email", email);
                            params.put("password", psw);
                            Log.d("LoginREQUESTDATA ", "PARAMS " + params.toString());
                            return new JSONObject(params).toString().getBytes();
                        }*/
            };

            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(stringRequest);

        } else {
            Snackbar snackbar = Snackbar.make(view, "No Internet Connection...!", Snackbar.LENGTH_LONG);
            snackbar.show();
        }

    }

    private void getPopularProducts(final String loc_id) {


        checkInternet = NetworkChecking.isConnected(getActivity());
        if (checkInternet) {

            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.GET_POPULAR_PRODUCTS,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("ksdnsdsdagsa", response);

                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {


                                    JSONObject json = jsonObject.getJSONObject("data");
                                    JSONArray jsonArray = json.getJSONArray("recordData");

                                    for (int i = 0; i < jsonArray.length(); i++) {

                                        MostSellingModel gbm = new MostSellingModel();
                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        gbm.setId(jsonObject1.getString("id"));
                                        gbm.setName(jsonObject1.getString("name"));
                                        gbm.setUrl_name(jsonObject1.getString("url_name"));
                                        gbm.setCount_id(jsonObject1.getString("count_id"));
                                        gbm.setWeight_id(jsonObject1.getString("weight_id"));
                                        gbm.setCount_name(jsonObject1.getString("count_name"));
                                        gbm.setWeight_name(jsonObject1.getString("weight_name"));
                                        gbm.setQty(jsonObject1.getString("qty"));
                                        gbm.setMrp_price(jsonObject1.getString("mrp_price"));
                                        gbm.setImages(AppUrls.PRODUCTS_IMAGE_URL + jsonObject1.getString("images"));
                                        gbm.setFeatures(jsonObject1.getString("features"));
                                        gbm.setStatus(jsonObject1.getString("status"));
                                        gbm.setUser_rating(jsonObject1.getString("user_rating"));


                                        popularproductlist.add(gbm);

                                    }

                                    papular_products_recyclerview.setAdapter(popularproductadapter);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {


                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {


                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("location_id", loc_id);
                    params.put("page_no", "1");
                    Log.d("LoginREQUESTDATA:", params.toString());
                    return params;
                }
                       /* @Override
                        public byte[] getBody() throws AuthFailureError
                        {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("email", email);
                            params.put("password", psw);
                            Log.d("LoginREQUESTDATA ", "PARAMS " + params.toString());
                            return new JSONObject(params).toString().getBytes();
                        }*/
            };

            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(stringRequest);

        } else {
            Snackbar snackbar = Snackbar.make(view, "No Internet Connection...!", Snackbar.LENGTH_LONG);
            snackbar.show();
        }

    }

}
