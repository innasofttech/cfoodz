package innasoft.in.cfoodz.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.labo.kaji.fragmentanimations.CubeAnimation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import innasoft.in.cfoodz.R;
import innasoft.in.cfoodz.adapter.SlidingImage_Adapter;
import innasoft.in.cfoodz.models.CityModel;
import innasoft.in.cfoodz.models.MainCategoryModel;
import innasoft.in.cfoodz.utilities.AppUrls;
import innasoft.in.cfoodz.utilities.CitySelectionSession;
import innasoft.in.cfoodz.utilities.NetworkChecking;

public class MainFragment extends Fragment {

    View view;

    private boolean checkInternet;
    public String disp_city_name = "", disp_city_id = "";
    CitySelectionSession citySelectionSession;
//    RecyclerView main_categ_recyclerView;
    ArrayList<MainCategoryModel> mainCategoryList = new ArrayList<>();
//    MainCategoryAdapter mainCategoryAdapter;
    int defaultPageNo = 1;
    int total_number_of_items = 0;
    private boolean loading = true;
    ArrayList<String> citi_list = new ArrayList<>();
    ArrayList<CityModel> homecityModels = new ArrayList<>();

    ViewPager mPager;

    public static MainFragment newInstance(int direction) {
        MainFragment f = new MainFragment();
        f.setArguments(new Bundle());
        f.getArguments().putInt("direction", direction);
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_main, container, false);


        mPager = (ViewPager) view.findViewById(R.id.pager);
        citySelectionSession = new CitySelectionSession(getActivity());


//        main_categ_recyclerView = (RecyclerView) view.findViewById(R.id.main_categ_recyclerView);
//        mainCategoryAdapter = new MainCategoryAdapter(mainCategoryList, MainFragment.this, R.layout.row_main_category);
//        // main_categ_recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL, false));
//        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getContext(), 1);
//        main_categ_recyclerView.setNestedScrollingEnabled(false);
//        main_categ_recyclerView.setLayoutManager(layoutManager);

        loadCityData();


        return view;
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        return CubeAnimation.create(CubeAnimation.UP, enter, 500);
    }


    private void getCityData() {
        checkInternet = NetworkChecking.isConnected(getActivity());
        if (checkInternet) {
            homecityModels.clear();
            StringRequest string_request = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.LOCATION,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {
//                            Log.e("RESPCITY", "RESPCITY:" + response.toString());
                            try {
                                JSONObject jsonObject = new JSONObject(response);
//                                Log.d("Citites:", response);
                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        CityModel pcm = new CityModel();
                                        pcm.setCityId(jsonObject1.getString("id"));
                                        String city_name = jsonObject1.getString("name");
                                        pcm.setCityName(city_name);
                                        citi_list.add(city_name);
                                        homecityModels.add(pcm);
                                    }
                                    //   cityDialog();
                                    citySelectionSession.createCity(homecityModels.get(0).getCityId(), homecityModels.get(0).getCityName());
                                    loadCityData();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getActivity(), "Error:" + error, Toast.LENGTH_LONG).show();
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    } else if (error instanceof AuthFailureError) {
                        //TODO
                    } else if (error instanceof ServerError) {
                        //TODO
                    } else if (error instanceof NetworkError) {

                    } else if (error instanceof ParseError) {
                        //TODO
                    }
                }
            });

            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(string_request);
        } else {
            Toast.makeText(getActivity(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void loadCityData() {
        if (!citySelectionSession.checkCityCreated()) {
            HashMap<String, String> cityDetails = citySelectionSession.getCityDetails();
            disp_city_id = cityDetails.get(CitySelectionSession.CITY_ID);
            disp_city_name = cityDetails.get(CitySelectionSession.CITY_NAME);

//            getSliders(disp_city_id);
            getMainCategory(defaultPageNo);

        } else {
            getCityData();
        }
    }

    private void getMainCategory(final int default_page_number) {
        checkInternet = NetworkChecking.isConnected(getActivity());
        if (checkInternet) {
            Log.d("MAINCATEGURL", AppUrls.BASE_URL + AppUrls.GET_MAIN_CATEGORY);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.GET_MAIN_CATEGORY,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("MAINCATEGRESP", response);
                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    JSONObject json = jsonObject.getJSONObject("data");
                                    int jsonArray_count = json.getInt("recordTotalCnt");
                                    JSONArray jsonArray = json.getJSONArray("recordData");
                                    total_number_of_items = jsonArray_count;
                                    Log.d("LOADSTATUS", "OUTER " + loading + "  " + jsonArray_count);
                                    if (jsonArray_count > mainCategoryList.size()) {
                                        loading = true;
                                        Log.d("LOADSTATUS", "INNER " + loading + "  " + jsonArray_count);
                                    } else {
                                        loading = false;
                                    }

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        MainCategoryModel gbm = new MainCategoryModel();
                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        gbm.setCategoryId(jsonObject1.getString("id"));
                                        gbm.setCategoryName(jsonObject1.getString("name"));
                                        mainCategoryList.add(gbm);
                                    }

//                                    main_categ_recyclerView.setAdapter(mainCategoryAdapter);

//                                    mPager.setPageTransformer(true, new CubeTransformer());
                                    mPager.setAdapter(new SlidingImage_Adapter(getActivity(),mainCategoryList));


                                }
                                if (responceCode.equals("12786")) {
                                    Toast.makeText(getActivity(), "No Data Found..!", Toast.LENGTH_SHORT).show();
                                }
                                if (responceCode.equals("11786")) {
                                    Toast.makeText(getActivity(), "All fields are required..!", Toast.LENGTH_SHORT).show();
                                }
                                if (responceCode.equals("10786")) {
                                    Toast.makeText(getActivity(), "Invalid Token.!", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {


                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("page_no", String.valueOf(default_page_number));
                    Log.d("MainCATEREQUESTDATA:", params.toString());
                    return params;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(stringRequest);

        } else {
            Toast.makeText(getActivity(), "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }

    }
}

