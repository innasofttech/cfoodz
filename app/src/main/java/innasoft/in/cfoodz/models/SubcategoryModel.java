package innasoft.in.cfoodz.models;

public class SubcategoryModel
{
    public String id;
    public String name;
    public String child_available;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getChild_available() {
        return child_available;
    }

    public void setChild_available(String child_available) {
        this.child_available = child_available;
    }
}
