package innasoft.in.cfoodz.models;

/**
 * Created by user on 17/10/2017.
 */

public class GetBannerModel {
    public String image;
    public String id;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
