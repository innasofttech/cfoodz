package innasoft.in.cfoodz.dbhelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import innasoft.in.cfoodz.models.CartListModel;

/**
 * Created by purushotham on 23/3/17.
 */

public class AddcartDatabase extends SQLiteOpenHelper
{

    private static final String DATABASE_NAME = "CartDb";
    private static final int DATABASE_VERSION = 1;
    private static final String TABLE_NAME = "CartTable";
    String TAG = "MessageDatabase";
    private Context context;
    private static final String INSERT = "insert into " + TABLE_NAME + "(product_id,product_name,quantity,price) values (?,?,?,?)";

    public AddcartDatabase(Context context) {

        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {

        db.execSQL("CREATE TABLE "	+ TABLE_NAME + " (product_id INTEGER " + ",product_name VARCHAR2"+ ",quantity VARCHAR2"+ ",price VARCHAR2"+ ")");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w("Example",
                "Upgrading database, this will drop tables and recreate.");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public void insert(String product_id, String product_name, String quantity, String price) {

        SQLiteDatabase db = getWritableDatabase();


        ContentValues values =  new ContentValues();
        values.put(product_id, product_id);
        values.put(product_name, product_name);
        values.put(quantity,quantity);
        values.put(price,price);

         db.insert(TABLE_NAME, null, values);
       // Log.e("iNSERT cOUNT =", ""+i);

        db.close();
    }

    public void deleteAll(String hotel_id) {

        SQLiteDatabase db = getWritableDatabase();

        String where = "hotel_id = '" + hotel_id + "'";
        db.delete(TABLE_NAME, where, null);

        db.close();
    }

    public void deleteItem(String hotel_id, String c_id) {

        SQLiteDatabase db = getWritableDatabase();

        String where = "hotel_id = '" + hotel_id + "' AND " + "c_id = '" + c_id + "'";
        db.delete(TABLE_NAME, where, null);

        db.close();
    }

    public void deleteItem(String hotel_id, String c_id, long time) {

        SQLiteDatabase db = getWritableDatabase();

        String where = "hotel_id = '" + hotel_id + "' AND " + "c_id = '" + c_id + "' AND " + "time = " + time;
        db.delete(TABLE_NAME, where, null);

        db.close();
    }

    public ArrayList<CartListModel> getMessageListByCID(String hotel_id, String c_id) {

        SQLiteDatabase db = getReadableDatabase();

        ArrayList<CartListModel> aryExtraList = new ArrayList<CartListModel>();
        String where = "hotel_id='" + hotel_id + "' AND c_id='" + c_id + "'";
        String orderBy = "time DESC";
        Cursor cursor = db.query(TABLE_NAME, new String[] {
                "title", "body", "time", "state" }, where, null, null, null, orderBy);
        if (cursor.moveToFirst())
        {
            do {
                CartListModel info = new CartListModel();

               // info.prd_id = cursor.getString(0);
              //  info.prd_name = cursor.getString(1);
             //   info.prd_quantity = cursor.getString(2);
             //   info.prd_price =  cursor.getString(3);


                aryExtraList.add(info);
            } while (cursor.moveToNext());
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }

        db.close();
        return aryExtraList;
    }

    public Cursor getUnreadCount() {

        SQLiteDatabase db = getReadableDatabase();

        int count = 0;
        Cursor cursor = null;
        try {
            String qry = "SELECT * FROM " + TABLE_NAME ;
            cursor = db.rawQuery(qry, null);
            while (cursor.moveToNext()) {
                count = cursor.getInt(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        db.close();
        //	return count;
        return cursor;
    }


}
